__author__ = 'winzo'
# -*- coding: utf-8 -*-

from flask import Blueprint, current_app
from flask.ext.mail import Mail

blueprint = Blueprint('emails', __name__)

mail = Mail()


@blueprint.before_app_first_request
def mailer_load():
    mail.init_app(current_app)

