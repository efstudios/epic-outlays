# -*- coding: utf-8 -*-
__author__ = 'winzo'

import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Mailer(object):
    def __init__(self):
        self.email_from = None
        self.connection = None

        self.host = None
        self.port = None
        self.user = None
        self.password = None
        self.ssl = False

    def _connect(self):
        if self.ssl:
            from smtplib import SMTP_SSL as SMTP
        else:
            from smtplib import SMTP

        try:
            self.connection = SMTP(self.host, int(self.port))
        except Exception, e:
            logging.critical(e.value)

        self.connection.ehlo()
        self.connection.login(self.user, self.password)

    def connect(self, host, port, user, password, ssl=False):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.ssl = ssl
        self._connect()

    def _send(self, emails_to, message, repeat=0):
        try:
            self.connection.sendmail(self.email_from, emails_to, message.as_string())
        except smtplib.SMTPRecipientsRefused, e:
            logging.critical('Invalid email')
        except smtplib.SMTPServerDisconnected, e:
            if repeat > 2:
                logging.critical('Could not connect to server')
                return False
            self._connect()
            self._send(emails_to, message, repeat + 1)

        return True

    def send(self, emails_to, subject, text):
        if not self.host or not self.port or not self.user or not self.password:
            logging.critical('Please, connect to server before send emails')
            return False

        # Create message container - the correct MIME type is multipart/alternative.
        message = MIMEMultipart('alternative')
        message['From'] = self.email_from
        message['Subject'] = subject
        message['To'] = emails_to

        # Record the MIME types of both parts - text/plain and text/html.
        text_part = MIMEText(text.encode('utf-8'), 'html', 'utf-8')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        message.attach(text_part)

        # attachments
        '''for attachment in attachments:
            mime = attachment[self.config['attachment_table']['columns']['content_type']].split('/') or attachment[self.config['attachment_table']['columns']['default_content_type']].split('/')
            part = MIMEBase(mime[0], mime[1])
            part.set_payload( attachment[self.config['attachment_table']['columns']['data']] )
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % attachment[self.config['attachment_table']['columns']['filename']].encode('utf-8'))
            msg.attach(part)'''

        self._send(emails_to, message)