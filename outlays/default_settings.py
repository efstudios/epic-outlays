# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

STANDALONE_HOST = 'localhost'
STANDALONE_PORT = 5001
DATABASE_URI = 'postgresql://outlays:outlays@localhost:5432/outlays'

# SECRET_KEY = ''
