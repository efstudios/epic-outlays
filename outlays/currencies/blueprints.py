# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from flask import Blueprint, g
from sqlalchemy import or_
from outlays.api.decorators import api_func, login_required
from outlays.api.proto import ApiResponse
from .models import Currency


blueprint = Blueprint('currencies', __name__)


@blueprint.route('', methods=['GET'])
@login_required
@api_func
def get_several_currencies():
    """ Get all available currencies
    """
    currencies = g.session.query(Currency)
    currencies_list = [c.serialize() for c in currencies]

    return ApiResponse(page=1, total_count=currencies.count(), items=currencies_list)


@blueprint.route('/<currency_id>', methods=['GET'])
@login_required
@api_func
def get_currency(currency_id):
    """ Get currency by @currency_id
    """
    return ApiResponse(**g.session.query(Currency).get(currency_id).serialize())


@blueprint.route('', methods=['POST'])
@login_required
@api_func
def create_currency(code, sign, denominator, description=None):
    """ Add a new custom currency.
    """
    new_currency = Currency(
        code=code,
        sign=sign,
        denominator=denominator,
        description=description,
        user_id=g.user.id
    )
    g.session.add(new_currency)
    g.session.commit()
    return ApiResponse(**new_currency.serialize())


@blueprint.route('/<currency_id>', methods=['PUT'])
@login_required
@api_func
def edit_currency(currency_id, code=None, sign=None, denominator=None, description=None):
    """ Change an existing custom currency.
    """
    currency = g.session.query(Currency).get(currency_id)

    if code is not None:
        currency.code = code

    if sign is not None:
        currency.sign = sign

    if denominator is not None:
        currency.denominator = denominator

    if description is not None:
        currency.description = description

    g.session.commit()

    return ApiResponse(**currency.serialize())


@blueprint.route('/<currency_id>', methods=['DELETE'])
@login_required
@api_func
def delete_currency(currency_id):
    """ Delete an existing custom currency.
    """
    currency = g.session.query(Currency).get(currency_id)
    g.session.delete(currency)
    g.session.commit()

    return ApiResponse()