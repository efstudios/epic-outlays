# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from outlays.db.blueprints import Base
from outlays.db.models import SerializableModel
from outlays.users.models import User


class Currency(SerializableModel, Base):
    """ CREATE TABLE IF NOT EXISTS "currencies" (
            "id" SERIAL PRIMARY KEY,
            "code" varchar(3) NOT NULL,
            "description" varchar(255),
            "denominator" integer NOT NULL,
            "sign" varchar(1) NOT NULL,
            "user_id" integer references users(id) DEFAULT NULL
        );
    """
    __tablename__ = 'currencies'

    id = Column(Integer, primary_key=True)
    code = Column(String(3), nullable=False)
    description = Column(String(255), default=None)
    denominator = Column(Integer, nullable=False)
    sign = Column(String(1), nullable=False)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship(User)

    def __init__(self, *args, **kwargs):
        SerializableModel.__init__(self)
        Base.__init__(self, *args, **kwargs)