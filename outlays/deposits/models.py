# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from outlays.db.blueprints import Base
from outlays.db.models import SerializableModel
from outlays.users.models import User
from outlays.currencies.models import Currency


class DepositType(SerializableModel, Base):
    """ CREATE TABLE IF NOT EXISTS "deposit_types" (
            "id" SERIAL PRIMARY KEY,
            "name" varchar(20) NOT NULL,
            "description" varchar(255)
        );
    """
    __tablename__ = 'deposit_types'

    id = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False)
    description = Column(String(255), default=None)

    def __init__(self, *args, **kwargs):
        SerializableModel.__init__(self)
        Base.__init__(self, *args, **kwargs)


class Deposit(SerializableModel, Base):
    """ CREATE TABLE IF NOT EXISTS "deposits" (
            "id" SERIAL PRIMARY KEY,
            "type_id" integer references deposit_types(id) NOT NULL,
            "currency_id" integer references currencies(id) NOT NULL,
            "value" integer NOT NULL,
            "value_min" integer DEFAULT NULL,
            "value_max" integer DEFAULT NULL,
            "description" varchar(255),
            "creation_date" timestamp with time zone DEFAULT current_timestamp NOT NULL,
            "last_modified" timestamp with time zone DEFAULT current_timestamp NOT NULL,
            "deletion_date" timestamp with time zone DEFAULT NULL,
            "date" timestamp with time zone DEFAULT current_timestamp NOT NULL,
            "user_id" integer references users(id) NOT NULL,
            CHECK (value >= value_min),
            CHECK (value <= value_max)
        );
    """
    __tablename__ = 'deposits'  # name of table in database
    __date_field__ = 'date'     # name of field for short-filters by date

    id = Column(Integer, primary_key=True)
    type_id = Column(Integer, ForeignKey('deposit_types.id'))
    currency_id = Column(Integer, ForeignKey('currencies.id'))
    value = Column(Integer, nullable=False)
    value_min = Column(Integer, default=None)
    value_max = Column(Integer, default=None)
    description = Column(String(255), default=None)
    creation_date = Column(DateTime, nullable=False, server_default='current_timestamp')
    last_modified = Column(DateTime, nullable=False, server_default='current_timestamp')
    deletion_date = Column(DateTime, default=None)
    date = Column(DateTime, nullable=False, server_default='current_timestamp')
    user_id = Column(Integer, ForeignKey('users.id'))
    type = relationship(DepositType)
    currency = relationship(Currency)
    user = relationship(User)

    def __init__(self, *args, **kwargs):
        SerializableModel.__init__(self)
        Base.__init__(self, *args, **kwargs)