# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from flask import Blueprint, g, request
from outlays.api.decorators import api_func, login_required
from outlays.api.proto import ApiResponse, EasyDate, DoesNotExist
from .models import Deposit, DepositType


blueprint = Blueprint('deposits', __name__)


@blueprint.route('', methods=['GET'])
@login_required
@api_func
def get_several_deposits(
    date_after=None, date_before=None,
    value_min=None, value_max=None,
    page=1, page_size=None, page_contains=None,
    description=None, currency_id=None, type_id=None, period=None
):
    """ Get several deposits
    """
    deposits = g.session.query(Deposit).all()

    if period is not None:
        deposits = deposits.by_period(period)

    if date_after is not None:
        deposits = deposits.filter(Deposit.date >= EasyDate.from_str(date_after))

    if date_before is not None:
        deposits = deposits.filter(Deposit.date <= EasyDate.from_str(date_before))

    if value_min is not None:
        deposits = deposits.filter(Deposit.value >= value_min)

    if value_max is not None:
        deposits = deposits.filter(Deposit.value <= value_max)

    if description is not None:
        deposits = deposits.filter(Deposit.description.contains(description))

    if currency_id is not None:
        deposits = deposits.filter_by(currency_id=currency_id)

    if type_id is not None:
        deposits = deposits.filter_by(type_id=type_id)

    for field in request.values.getlist('sort_by'):
            deposits = deposits.order_by(field)

    total_count = deposits.count()

    if page_size is not None:

        if page_contains is not None:
            position = 0
            for transaction in deposits.all():
                if int(page_contains) == transaction.id:
                    break

                position += 1

            else:
                raise DoesNotExist('Deposit that you requested does not exist.')

            page = (position // int(page_size)) + 1

        deposits = deposits\
            .limit(int(page_size))\
            .offset((int(page) - 1) * int(page_size))

    deposits_list = [d.serialize() for d in deposits]

    return ApiResponse(page=int(page), total_count=total_count, items=deposits_list)


@blueprint.route('/<deposit_id>', methods=['GET'])
@login_required
@api_func
def get_deposit(deposit_id):
    """ Get deposit by @deposit_id
    """
    return ApiResponse(**g.session.query(Deposit).get(deposit_id).serialize())


@blueprint.route('', methods=['POST'])
@login_required
@api_func
def create_deposit(type_id, currency_id, value, value_min=None, value_max=None, description=None, date=None):
    """ Add a new custom deposit.
    """
    new_deposit = Deposit(
        type_id=type_id,
        currency_id=currency_id,
        value=value,
        value_min=value_min,
        value_max=value_max,
        description=description,
        date=EasyDate.from_str(date) if date is not None else EasyDate.now(),
        user_id=g.user.id
    )
    g.session.add(new_deposit)
    g.session.commit()
    return ApiResponse(**new_deposit.serialize())


@blueprint.route('/<deposit_id>', methods=['PUT'])
@login_required
@api_func
def edit_deposit(
    deposit_id, type_id=None, currency_id=None, value=None, value_min=None, value_max=None, description=None, date=None
):
    """ Change an existing custom deposit.
    """
    deposit = g.session.query(Deposit).get(deposit_id)

    if type_id is not None:
        deposit.type_id = type_id

    if currency_id is not None:
        deposit.currency_id = currency_id

    if value is not None:
        deposit.value = value

    if value_min is not None:
        deposit.value_min = value_min

    if value_max is not None:
        deposit.value_max = value_max

    if description is not None:
        deposit.description = description

    if date is not None:
        deposit.date = EasyDate.from_str(date)

    g.session.commit()
    return ApiResponse(**deposit.serialize())


@blueprint.route('/<deposit_id>', methods=['DELETE'])
@login_required
@api_func
def delete_deposit(deposit_id):
    """ Delete an existing custom deposit.
    """
    deposit = g.session.query(Deposit).get(deposit_id)
    deposit.deletion_date = EasyDate.now()
    g.session.commit()

    return ApiResponse(**deposit.serialize())


@blueprint.route('_restore/<deposit_id>', methods=['PUT'])
@login_required
@api_func
def restore_deposit(deposit_id):
    """ Restore an existing custom deposit.
    """
    deposit = g.session.query(Deposit).get(deposit_id)
    deposit.deletion_date = None
    g.session.commit()

    return ApiResponse(**deposit.serialize())


@blueprint.route('_types', methods=['GET'])
@login_required
@api_func
def get_types():
    """ Get list of deposit types
    """
    types = g.session.query(DepositType)
    types_list = [t.serialize() for t in types]

    return ApiResponse(page=1, total_count=types.count(), items=types_list)