# -*- coding: utf-8 -*-
__author__ = 'winzo'

from outlays.api.proto import DateTimeEncoder
import json
from outlays.currencies.models import Currency
from outlays.deposits.models import Deposit, DepositType
from outlays.users import user_manager
from outlays.transactions.blueprints import blueprint as transactions_blueprint
from outlays.deposits.blueprints import blueprint as deposits_blueprint
from outlays.currencies.blueprints import blueprint as currencies_blueprint
from outlays.users.blueprints import blueprint as users_blueprint
from outlays.emails.blueprints import blueprint as emails_blueprint
from outlays.db.blueprints import blueprint as db_blueprint
from flask import Flask, render_template, g

app = Flask(__name__)
app.config.from_object('outlays.default_settings')
app.config.from_envvar('OUTLAYS_SETTINGS')

app.register_blueprint(db_blueprint)

app.register_blueprint(emails_blueprint)

app.register_blueprint(users_blueprint, url_prefix='/users')

app.register_blueprint(currencies_blueprint, url_prefix='/api/currencies')

app.register_blueprint(deposits_blueprint, url_prefix='/api/deposits')

app.register_blueprint(transactions_blueprint, url_prefix='/api/transactions')


@app.route('/old')
@user_manager.require_authorization
def index_old():
    deposits = g.session.query(Deposit).all()
    deposits_list = [d.serialize() for d in deposits]

    currencies = g.session.query(Currency).all()
    currencies_list = [c.serialize() for c in currencies]

    return render_template('index.old.html', deposits=json.dumps(deposits_list, cls=DateTimeEncoder), currencies=json.dumps(currencies_list, cls=DateTimeEncoder))


def index():
    user = g.user.serialize() if g.user.is_authorized() else None
    deposit_types = [d.serialize() for d in g.session.query(DepositType).all()]
    deposits = [d.serialize() for d in g.session.query(Deposit).all()]
    currencies = [c.serialize() for c in g.session.query(Currency).all()]

    return render_template(
        'index.html',
        user=json.dumps(user, cls=DateTimeEncoder),
        deposits=json.dumps(deposits, cls=DateTimeEncoder),
        currencies=json.dumps(currencies, cls=DateTimeEncoder),
        deposit_types=json.dumps(deposit_types, cls=DateTimeEncoder)
    )


@app.route('/')
def react_index():
    return index()


@app.route('/transactions')
def react_transactions():
    return index()


@app.route('/transactions/<transaction_id>')
def react_transaction(transaction_id):
    return index()


@app.route('/deposits')
def react_deposits():
    return index()


@app.route('/deposits/<deposit_id>')
def react_deposit(deposit_id):
    return index()


@app.errorhandler(403)
def permissions_required(_):
    return render_template('403.html'), 403


@app.errorhandler(404)
def page_not_found(_):
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(
        app.config['STANDALONE_HOST'],
        app.config['STANDALONE_PORT']
    )
