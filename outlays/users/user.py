
# -*- coding: utf-8 -*-
__author__ = 'winzo'

from flask import session, g, abort, current_app, redirect, url_for, request
from functools import wraps
from hashlib import sha1
from outlays.db.models import SerializableModel


class UserManager():
    def __init__(self):
        pass

    def require_authorization(self, f):
        @wraps(f)
        def _inner_req_auth(*args, **kwargs):
            if g.user.is_authorized():
                return f(*args, **kwargs)
            else:
                redirect_to = '/'.join(request.url.split('/')[3:])
                return redirect(url_for('users.login', next=redirect_to))
        return _inner_req_auth


class User:
    def __init__(self):
        self.id = None
        self._attrs = []
        if 'user' in session:
            self.load()

    def load(self, user=None):
        if user is None:
            user = session['user']

        if isinstance(user, SerializableModel):
            user = user.serialize()

        self._attrs = [f for f in user]

        for f in user:
            setattr(self, f, user[f])

    def save(self):
        if 'user' not in session:
            session['user'] = {}

        for attr in self._attrs:
            session['user'][attr] = getattr(self, attr)

        session.modified = True

    def is_authorized(self):
        return self.id is not None

    def login(self, user):
        self.load(user)
        self.save()

    def logout(self):
        if 'user' in session:
            for attr in self._attrs:
                setattr(self, attr, None)
                if attr in session['user']:
                    session['user'][attr] = None
            del session['user']
            session.modified = True

    @staticmethod
    def hash_it(password):
        return sha1(current_app.secret_key + password.encode('utf-8')).digest()

    def serialize(self):
        """ Transforms a model into a dictionary which can be dumped to JSON.
        """
        return dict(
            id=self.id,
            name=self.name,
            last_login=self.last_login,
            email=self.email,
        )
