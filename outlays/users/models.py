# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.dialects.postgresql import BYTEA
from outlays.db.blueprints import Base
from outlays.db.models import SerializableModel
import uuid
from flask import g

class User(SerializableModel, Base):

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    password = Column(BYTEA(20), default=None)
    # Password field contains SHA1 digest of combination of password
    # and secret key. It is 160 bits represented as array of 20 bytes.
    email = Column(String(254), default=None)
    # The maximum of 256-character length of a forward or reverse path
    # restricts the entire email address to be no more than 254 characters long
    # (RFC 5322, RFC 5321, RFC 3696).
    phone = Column(String(15), default=None)
    # According to ITU-T recommendation E.164, international public
    # telecommunication number contains maximum 15 digits.
    registration_date = Column(DateTime, nullable=False, server_default='current_timestamp')
    last_modified = Column(DateTime, nullable=False, server_default='current_timestamp')
    deletion_date = Column(DateTime, default=None)
    last_login = Column(DateTime, nullable=False, server_default='current_timestamp')
    active = Column(Boolean, default=False)
    activation_code = Column(String(36))
    oauth_google = Column(String(512))

    def __init__(self, *args, **kwargs):
        SerializableModel.__init__(self)
        Base.__init__(self, *args, **kwargs)

    def gen_activation_code(self):
        activation_code = uuid.uuid4()
        while g.session.query(User).filter(User.activation_code == activation_code.hex).count():
            activation_code = uuid.uuid4()
        self.activation_code = activation_code.hex