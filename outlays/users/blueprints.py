
# -*- coding: utf-8 -*-
__author__ = 'winzo'


from flask import Blueprint, flash, request, redirect, url_for, g, render_template, current_app
from .form.signup import SignupForm
from .form.login import LoginForm
from .form.reset_password import ResetPasswordForm
from .form.activation_code import ActivationCodeForm
from .user import UserManager, User
from .models import User as UserModel
from flask.ext.mail import Message

from outlays.emails import mail


blueprint = Blueprint('users', __name__, template_folder='templates', static_folder='static')
user_manager = UserManager()


@blueprint.before_app_request
def load_user():
    g.user = User()


@blueprint.teardown_app_request
def save_user(response):
    g.user.save()
    return response


@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    next_url = request.args.get('next') or url_for('index')

    if g.user.is_authorized():
        return redirect('/' + next_url)

    form = LoginForm(request.form, csrf_enabled=False)
    if request.method == 'POST' and form.validate():

        user = g.session.query(UserModel)\
            .filter(UserModel.email == form.email.data)\
            .filter(UserModel.password == g.user.hash_it(form.password.data))\
            .first()

        if user:
            g.user.login(user)
            flash(u'You signed in as %s' % (g.user.name), 'success')
            return redirect('/' + next_url)

        flash(u'Invalid email and/or password', 'danger')

    if request.method == 'POST' and not form.validate():
        flash(u'Log in failed. See errors in form', 'danger')

    return render_template('users/login.html', form=form)


@blueprint.route('/signup', methods=['GET', 'POST'])
def signup():
    next_url = request.args.get('next') or url_for('index')

    if g.user.is_authorized():
        return redirect('/' + next_url)

    form = SignupForm(request.form, csrf_enabled=False)
    if request.method == 'POST' and form.validate():

        user = UserModel()
        user.name = form.name.data
        user.email = form.email.data
        user.phone = form.phone.data

        user.password = g.user.hash_it(form.password.data)

        g.session.add(user)
        g.session.commit()

        g.user.login(user)

        flash(u'You signed in as %s' % (g.user.name), 'success')
        return redirect('/' + next_url)

    if request.method == 'POST' and not form.validate():
        flash(u'Signup failed. See errors in form', 'danger')

    return render_template('users/signup.html', form=form)


@blueprint.route('/logout', methods=['GET'])
def logout():
    g.user.logout()
    return redirect(url_for('index'))


@blueprint.route('/reset_password', methods=['GET', 'POST'])
def activation_code():
    if g.user.is_authorized():
        return redirect(url_for('index'))

    form = ActivationCodeForm(request.form, csrf_enabled=False)

    if request.method == 'POST' and form.validate():
        user = g.session.query(UserModel).filter(UserModel.email == form.email.data).first()

        if not user:
            flash(u'User does not exists', 'danger')
            return render_template('users/activation_code.html', form=form)

        user.gen_activation_code()
        g.session.commit()

        # send message
        msg = Message('[Epic Outlays] Password reset', recipients=[user.email], sender=current_app.config['DEFAULT_MAIL_SENDER'])
        msg.body = request.host_url[:-1] + url_for('.reset_password', code=user.activation_code)
        msg.html = '<a href="' + request.host_url[:-1] + url_for('.reset_password', code=user.activation_code) + '">Click</a> to set your new password'

        # HACK: mail.send(msg) should work but not
        with mail.connect() as conn:
            connection = conn.configure_host()
            connection.sendmail(current_app.config['DEFAULT_MAIL_SENDER'], user.email, msg.as_string())

        flash(u'Email with activation code sent', 'success')
        return redirect(url_for('index'))

    if request.method == 'POST' and not form.validate():
        flash(u'Enter a valid email', 'danger')

    return render_template('users/activation_code.html', form=form)


@blueprint.route('/reset_password/<code>', methods=['GET', 'POST'])
def reset_password(code):
    if g.user.is_authorized():
        return redirect(url_for('index'))

    user = g.session.query(UserModel)\
        .filter(UserModel.activation_code == code)\
        .first()

    if not user:
        return render_template('users/invalid_activation_code.html')

    form = ResetPasswordForm(request.form, csrf_enabled=False)

    if request.method == 'POST' and form.validate():
        if user.email == form.email.data:
            user.activation_code = None
            user.password = g.user.hash_it(form.password.data)

            g.session.commit()

            flash(u'Your password changed', 'success')

            g.user.login(user)
            return redirect(url_for('index'))
        else:
            flash(u'Enter a valid email', 'danger')

    if request.method == 'POST' and not form.validate():
        flash(u'Enter a valid data', 'danger')

    return render_template('users/reset_password.html', form=form)

