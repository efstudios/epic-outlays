# -*- coding: utf-8 -*-
__author__ = 'winzo'

from flask.ext.wtf import Form
from wtforms import TextField
from wtforms.validators import Required, Email


class ActivationCodeForm(Form):
    email = TextField(u'Email', validators=[Required(), Email()])