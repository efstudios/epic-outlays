# -*- coding: utf-8 -*-
__author__ = 'winzo'

from flask.ext.wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import Required, Optional, Email, EqualTo


class SignupForm(Form):
    name = TextField(u'Name', validators=[Required()])
    email = TextField(u'Email', validators=[Required(), Email()])

    password = PasswordField(u'Password', validators=[Required()])
    password_confirm = PasswordField(u'Password confirm',
                                     validators=[Required(), EqualTo('password', u'Passwords must be same')])

    phone = TextField(u'Phone', validators=[Optional()])
