# -*- coding: utf-8 -*-
__author__ = 'winzo'

from flask.ext.wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import Required, Email


class LoginForm(Form):
    email = TextField(u'Email', validators=[Required(), Email()])
    password = PasswordField(u'Password', validators=[Required()])