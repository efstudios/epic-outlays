# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

import logging
import flask
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from .models import EasyQuery
from flask import current_app


blueprint = flask.Blueprint('db', __name__)
Base = declarative_base()


@blueprint.before_app_first_request
def init_db():
    """ Creates the initial database connection
        Fired before the first HTTP request (to any part of the site).
    """
    current_app.db_engine = create_engine(current_app.config['DATABASE_URI'], convert_unicode=True)
    current_app.db_session = scoped_session(
        sessionmaker(
            autocommit=False,
            autoflush=False,
            bind=current_app.db_engine,
            query_cls=EasyQuery
        )
    )
    Base.query = current_app.db_session.query_property()


@blueprint.before_app_request
def connect():
    """ Creates a per request connection and transaction
    """
    flask.g.connection = current_app.db_engine.connect()
    flask.g.session = current_app.db_session()


@blueprint.teardown_app_request
def disconnect(exception):
    """ Commits or rolls back the transaction and disconnects
    """
    try:
        if exception is not None:
            flask.g.session.rollback()

        else:
            flask.g.session.commit()

        flask.g.connection.close()

    except Exception, e:
        logging.exception("Problem closing the DB: %r.", e)