# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from sqlalchemy.orm import class_mapper, Query
from outlays.api.proto import EasyDate, InvalidArgument, DoesNotExist, AccessDenied
from flask import g
from sqlalchemy import or_


class EasyQuery(Query):

    def __get_model__(self, index):
        return self._entities[index].type

    def by_period(self, period):

        model = self.__get_model__(0)
        date = getattr(model, model.__date_field__)
        now = EasyDate.now()

        if period == 'this_day':
            return self.filter(date >= now.date())

        elif period == 'this_week':
            return self.filter(date >= now.start_of_week())

        elif period == 'this_month':
            return self.filter(date >= now.start_of_month())

        elif period == 'this_year':
            return self.filter(date >= now.start_of_year())

        elif period == 'day':
            return self.filter(date >= now.day_ago())

        elif period == 'week':
            return self.filter(date >= now.week_ago())

        elif period == 'month':
            return self.filter(date >= now.month_ago())

        elif period == 'year':
            return self.filter(date >= now.year_ago())

        else:
            raise InvalidArgument(
                'Invalid period. Possible values: this_day|this_week|this_month|this_year|day|week|month|year'
            )

    def get(self, *args, **kwargs):
        queryset = Query.get(self, *args, **kwargs)
        model = self.__get_model__(0)
        if queryset is None:
            raise DoesNotExist('%s that you requested does not exist.' % model.__name__)

        if hasattr(model, 'deletion_date'):
            if queryset.deletion_date is not None:
                raise DoesNotExist('%s that you requested is already deleted.' % model.__name__)

        if hasattr(model, 'user_id'):
            if (queryset.user_id is not None) and (queryset.user_id != g.user.id):
                raise AccessDenied('Requested %s is not owned by authorized user.' % model.__name__.lower())

        return queryset

    def __wrap_filter__(self, queryset):
        model = self.__get_model__(0)
        if hasattr(model, 'deletion_date'):
            queryset = Query.filter(queryset, None == model.deletion_date)

        if hasattr(model, 'user_id'):
            queryset = Query.filter(queryset, or_(None == model.user_id, model.user_id == g.user.id))

        if hasattr(model, 'date'):
            queryset = Query.order_by(queryset, model.date.desc())

        return queryset

    def filter(self, *args, **kwargs):
        return self.__wrap_filter__(Query.filter(self, *args, **kwargs))

    def filter_by(self, **kwargs):
        return self.__wrap_filter__(Query.filter_by(self, **kwargs))

    def all(self):
        return self.__wrap_filter__(self)


class SerializableModel(object):

    def __get_columns__(self):
        """ Getting the names of all the columns on your model.
        """
        return [c.key for c in class_mapper(self.__class__).columns]

    def serialize(self):
        """ Transforms a model into a dictionary which can be dumped to JSON.
        """
        return dict((c, getattr(self, c)) for c in self.__get_columns__())