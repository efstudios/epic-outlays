# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from functools import wraps
import inspect
from .proto import MissingArgument, ApiMessage, InternalError, ApiException, LoginRequired
from flask import request, g
import logging


def api_func(func):
    """ The decorator for API function handler.
    """
    @wraps(func)
    def wrapper(**direct_args):
        """ Checks an API request arguments and returns an API response.
        """
        func_args, _, _, def_args = inspect.getargspec(func)

        arg_count = 0 if func_args is None else len(func_args)
        def_count = 0 if def_args is None else len(def_args)

        required_args = func_args[:arg_count - def_count]
        received_args = {arg: val  for arg, val in direct_args.items() + request.args.items() + request.form.items() if val}
        missing_args = [arg for arg in required_args if not arg in received_args]

        if missing_args:
            return MissingArgument('Arguments required: %s.' % ', '.join(missing_args)).as_response()

        passed_args = {arg: received_args[arg] for arg in func_args if arg in received_args}

        try:
            response = func(**passed_args)
            if isinstance(response, ApiMessage) or isinstance(response, ApiException):
                g.session.commit()
                return response.as_response()
            else:
                raise InternalError('This handler returns an invalid data.')

        except ApiException, e:
            g.session.rollback()
            return e.as_response()

        except Exception, e:
            g.session.rollback()
            logging.exception(e)
            return InternalError(e.message).as_response()

        finally:
            g.session.close()

    return wrapper


def login_required(func):
    """ The decorator for checking user authorization.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):

        if not g.user.is_authorized():
            return LoginRequired('You must be authorized to perform this request.').as_response()

        return func(*args, **kwargs)

    return wrapper