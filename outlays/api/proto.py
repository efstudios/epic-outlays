# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

import json
import re
from datetime import datetime
from flask import Response
from dateutil.relativedelta import relativedelta, MO
from dateutil.parser import parse as date_parser
import pytz


class EasyDate(datetime):
    """ The easy convertible datetime.
    """
    STR_FORMAT = '%Y-%m-%d %H:%M:%S'
    one_day = relativedelta(days=1)
    one_week = relativedelta(weeks=1)
    one_month = relativedelta(months=1)
    one_year = relativedelta(years=1)
    next_monday = relativedelta(weekday=MO)

    @staticmethod
    def from_str(s):
        return pytz.UTC.localize(date_parser(s))

    @classmethod
    def dt2str(cls, o):
        return o.astimezone(pytz.UTC).strftime(cls.STR_FORMAT)

    def to_str(self):
        return self.dt2str(self)

    def start_of_week(self):
        return (self.week_ago() + self.next_monday).date()

    def start_of_month(self):
        return self.date().replace(day=1)

    def start_of_year(self):
        return self.date().replace(month=1, day=1)

    def day_ago(self):
        return self - self.one_day

    def week_ago(self):
        return self - self.one_week

    def month_ago(self):
        return self - self.one_month

    def year_ago(self):
        return self - self.one_year

    def __init__(self, *args, **kwargs):
        super(datetime, self).__init__(*args, **kwargs)


class DateTimeEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime):
            return EasyDate.dt2str(obj)

        return json.JSONEncoder.default(self, obj)


class ApiMessage(dict):
    """ The basic API message class.
    """
    # fields
    STATUS_FIELD_NAME = 'status'
    DATA_FIELD_NAME = 'data'
    ERROR_FIELD_NAME = 'error'

    # statuses
    ST_DONE = 'DONE'
    ST_ERROR = 'ERROR'

    def __init__(self):
        super(dict, self).__init__()

    # representations
    def as_json(self):
        return json.dumps(self, cls=DateTimeEncoder)

    def as_response(self):
        return Response(
            self.as_json(),
            mimetype='application/json'
        )


class ApiResponse(ApiMessage):
    """ Constructor for API response.

        Work with multiple objects:
        >>> a = {'id': 1, 'deposit': 100}
        >>> b = {'id': 2, 'deposit': 500}
        >>> c = {'id': 5, 'deposit': 300}
        >>> ApiResponse(a, b, c)
        {'status': 'DONE', 'data': ({'id': 1, 'deposit': 100}, {'id': 2, 'deposit': 500}, {'id': 5, 'deposit': 300})}

        Work with single object:
        >>> ApiResponse(id=1, deposit=100)
        {'status': 'DONE', 'data': {'deposit': 100, 'id': 1}}

        WARNING: Do not mix these methods!

        Getting JSON:
        >>> ApiResponse(id=1, deposit=100).as_json()
        '{"status": "DONE", "data": {"deposit": 100, "id": 1}}'
    """
    def __init__(self, *args, **kwargs):
        ApiMessage.__init__(self)
        self[self.STATUS_FIELD_NAME] = self.ST_DONE
        self[self.DATA_FIELD_NAME] = kwargs if len(kwargs) else args


class ApiException(Exception):
    """ Prototype for API Exceptions.

        >>> class LoginRequired(ApiException): pass
        >>> try:
        ...     raise LoginRequired('Login to perform this request!')
        ... except ApiException, e:
        ...     print e.as_json()
        {"status": "ERROR", "error": {"kind": "LOGIN_REQUIRED", "desc": "Login to perform this request!"}}
    """

    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)
        self.__api_message = ApiMessage()
        self[self.__api_message.STATUS_FIELD_NAME] = self.__api_message.ST_ERROR
        self[self.__api_message.ERROR_FIELD_NAME] = {
            'kind': self.__error_name__(),
            'desc': self.message
        }

    def __error_name__(self):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', self.__class__.__name__)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).upper()

    def __getitem__(self, key):
        return self.__api_message.__getitem__(key)

    def __setitem__(self, key, value):
        return self.__api_message.__setitem__(key, value)

    def update(self, other, **kwargs):
        return self.__api_message.update(self, other, **kwargs)

    def as_json(self):
        return self.__api_message.as_json()

    def as_response(self):
        return self.__api_message.as_response()


class InternalError(ApiException):
    pass


class MaxLimitReached(ApiException):
    pass


class MinLimitReached(ApiException):
    pass


class MissingArgument(ApiException):
    pass


class InvalidArgument(ApiException):
    pass


class DoesNotExist(ApiException):
    pass


class LoginRequired(ApiException):
    pass


class AccessDenied(ApiException):
    pass


if __name__ == "__main__":
    import doctest
    doctest.testmod()