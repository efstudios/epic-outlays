# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from outlays.db.blueprints import Base
from outlays.db.models import SerializableModel
from outlays.users.models import User
from outlays.deposits.models import Deposit


class Transaction(SerializableModel, Base):
    """ CREATE TABLE IF NOT EXISTS "transactions" (
            "id" SERIAL PRIMARY KEY,
            "deposit_id" integer references deposits(id) NOT NULL,
            "value" integer NOT NULL,
            "description" varchar(255),
            "creation_date" timestamp with time zone DEFAULT current_timestamp NOT NULL,
            "last_modified" timestamp with time zone DEFAULT current_timestamp NOT NULL,
            "date" timestamp with time zone DEFAULT current_timestamp NOT NULL,
            "user_id" integer references users(id) NOT NULL
        );
    """
    __tablename__ = 'transactions'  # name of table in database
    __date_field__ = 'date'         # name of field for short-filters by date

    id = Column(Integer, primary_key=True)
    deposit_id = Column(Integer, ForeignKey('deposits.id'))
    value = Column(Integer, nullable=False)
    description = Column(String(255), default=None)
    creation_date = Column(DateTime, nullable=False, server_default='current_timestamp')
    last_modified = Column(DateTime, nullable=False, server_default='current_timestamp')
    date = Column(DateTime, nullable=False, server_default='current_timestamp')
    tags = Column(String(255), default=None)
    user_id = Column(Integer, ForeignKey('users.id'))
    deposit = relationship(Deposit)
    user = relationship(User)

    def __init__(self, *args, **kwargs):
        SerializableModel.__init__(self)
        Base.__init__(self, *args, **kwargs)