# -*- coding: utf-8 -*-
__author__ = 'SharUpOff <sharupoff@efstudios.org>'

from flask import Blueprint, g, request
from outlays.api.decorators import api_func, login_required
from outlays.api.proto import ApiResponse, EasyDate, DoesNotExist
from outlays.deposits.models import Deposit
from .models import Transaction


blueprint = Blueprint('transactions', __name__)


@blueprint.route('', methods=['GET'])
@login_required
@api_func
def get_several_transactions(
    date_after=None, date_before=None,
    value_min=None, value_max=None,
    direction='all', page=1, page_size=None, page_contains=None,
    description=None, deposit_id=None, period=None
):
    """ Get several transactions
    """
    deposit_list = [d.id for d in g.session.query(Deposit).all()]

    transactions = g.session.query(Transaction).filter(Transaction.deposit_id.in_(deposit_list))

    if period is not None:
        transactions = transactions.by_period(period)

    if date_after is not None:
        transactions = transactions.filter(Transaction.date >= EasyDate.from_str(date_after))

    if date_before is not None:
        transactions = transactions.filter(Transaction.date <= EasyDate.from_str(date_before))

    if value_min is not None:
        transactions = transactions.filter(Transaction.value >= value_min)

    if value_max is not None:
        transactions = transactions.filter(Transaction.value <= value_max)

    if direction == 'in':
        transactions = transactions.filter(Transaction.value > 0)

    elif direction == 'out':
        transactions = transactions.filter(Transaction.value < 0)

    if description is not None:
        transactions = transactions.filter(Transaction.description.contains(description))

    if deposit_id is not None:
        transactions = transactions.filter_by(deposit_id=deposit_id)

    for field in request.values.getlist('sort_by'):
        transactions = transactions.order_by(field)

    total_count = transactions.count()
    total_outlay = sum(t.value for t in transactions.filter(Transaction.value < 0).all())
    total_income = sum(t.value for t in transactions.filter(Transaction.value > 0).all())
    total_profit = total_income + total_outlay

    if page_size is not None:

        if page_contains is not None:
            position = 0
            for transaction in transactions.all():
                if int(page_contains) == transaction.id:
                    break

                position += 1

            else:
                raise DoesNotExist('Transaction that you requested does not exist.')

            page = (position // int(page_size)) + 1

        transactions = transactions\
            .limit(int(page_size))\
            .offset((int(page) - 1) * int(page_size))

    transactions_list = [t.serialize() for t in transactions]

    return ApiResponse(
        page=int(page),
        items=transactions_list,
        total_count=total_count,
        total_outlay=total_outlay,
        total_income=total_income,
        total_profit=total_profit
    )


@blueprint.route('/<transaction_id>', methods=['GET'])
@login_required
@api_func
def get_transaction(transaction_id):
    """ Get transaction info
    """
    return ApiResponse(**g.session.query(Transaction).get(transaction_id).serialize())


@blueprint.route('', methods=['POST'])
@login_required
@api_func
def create_transaction(deposit_id, value, description=None, tags=None, date=None):
    """ Add a new custom transaction.
    """
    new_transaction = Transaction(
        deposit_id=deposit_id,
        value=value,
        description=description,
        tags=tags,
        date=EasyDate.from_str(date) if date is not None else EasyDate.now(),
        user_id=g.user.id
    )
    g.session.add(new_transaction)
    g.session.commit()
    return ApiResponse(**new_transaction.serialize())


@blueprint.route('/<transaction_id>', methods=['PUT'])
@login_required
@api_func
def edit_transaction(transaction_id, deposit_id=None, value=None, description=None, date=None):
    """ Change an existing custom transaction.
    """
    transaction = g.session.query(Transaction).get(transaction_id)

    if deposit_id is not None:
        transaction.deposit_id = deposit_id

    if value is not None:
        transaction.value = value

    if description is not None:
        transaction.description = description

    if date is not None:
        transaction.date = EasyDate.from_str(date)

    g.session.commit()
    return ApiResponse(**transaction.serialize())


@blueprint.route('/<transaction_id>', methods=['DELETE'])
@login_required
@api_func
def delete_transaction(transaction_id):
    """ Delete an existing custom transaction.
    """
    transaction = g.session.query(Transaction).get(transaction_id)
    g.session.delete(transaction)
    g.session.commit()
    return ApiResponse()