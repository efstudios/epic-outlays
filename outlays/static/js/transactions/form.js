/**
 * Created by winzo on 19.04.14.
 */

var TransactionForm = Backbone.View.extend({

    tagName: 'form',
    className: 'form-horizontal',
    template: _.template($('#template-transaction-form').html()),

    events: {
        'change [name=deposit_id]': function() {
            this.model.set('deposit_id', parseInt(this.$el.find('[name=deposit_id]').val()))
        },
        'change [name=value]': function() {
            var value = parseFloat(this.$el.find('[name=value]').val());

            if (this.model.get('deposit_id')) {
                var deposit = window.collections.deposits.id(this.model.get('deposit_id'));
                var currency = window.collections.currencies.id(deposit.get('currency_id'));
                value = Math.round(value * currency.get('denominator'));
            }

            this.model.set('value', value);
        },
        'change [name=description]': function() {
            this.model.set('description', this.$el.find('[name=description]').val())
        },
        'change [name=date]': function() {
            this.model.set('date', this.$el.find('[name=date]').val())
        },
        'click [role=delete]': 'remove',
        'click [role=save]': 'save'
    },

    initialize: function() {
        var deposit_id = null;
        if (this.model.get('deposit_id')) {
            deposit_id = this.model.get('deposit_id');
        }

        this.listenTo(this.model, 'invalid', function(model, error) {
            console.error(error);
            alert(error);
        });

        this.listenTo(this.model, 'sync', function()
        {
            if (deposit_id) {
                window.collections.deposits.id(deposit_id).fetch();
            }
            if (this.model.get('deposit_id')) {
                window.collections.deposits.id(this.model.get('deposit_id')).fetch();
            }

            this.render();
        }, this);
    },

    save: function(event) {
        var btn = $(event.target);//this.$el.find('[role=save]');
        var error_text = this.$el.find('[role=save_error]');

        if (btn.hasClass('disabled')) {
            return;
        }

        btn.addClass('loading disabled');
        error_text.html('');

        this.model.once('invalid', function(){
            btn.removeClass('loading');
            btn.removeClass('disabled');
        }, this);

        var data = {};
        var date_input = this.$el.find('[name=date]').val();
        if (date_input && !this.model.get('date')) {
            data['date'] = date_input;
        }

        this.model.save(data, {
            success: function() {
                btn.removeClass('loading');
                btn.removeClass('disabled');
                navigate('transactions', true);
            },
            error: function(model, error) {
                btn.removeClass('loading');
                btn.removeClass('disabled');
                error_text.html(error.desc);
            }
        });
    },

    remove: function(event) {
        var btn = $(event.target);
        var error_text = this.$el.find('[role=save_error]');

        if (btn.hasClass('disabled')) {
            return;
        }

        btn.addClass('loading disabled');
        error_text.html('');

        this.model.destroy({
            success: function() {
                btn.removeClass('loading');
                btn.removeClass('disabled');
                navigate('transactions', true);
            },
            error: function(model, error) {
                btn.removeClass('loading');
                btn.removeClass('disabled');
                error_text.html(error.desc);
            }
        });
    },

    setDeposits: function(html) {
        var deposit = html.find('[name=deposit_id]');
        deposit.html('');

        var option = $('<option value=""></option>');
        deposit.append(option);

        for (var i = 0; i < window.collections.deposits.models.length; i++) {
            var model = window.collections.deposits.models[i];

            option = $('<option value="'+ model.get('id') +'">'+ model.__str__() +'</option>');

            if (model.get('id') == this.model.get('deposit_id')) {
                option.attr('selected', '');
            }

            deposit.append(option);
        }
    },

    updateValueOnDepositChange: function(transaction, deposit_id)
    {
        var old_deposit_id = transaction.previousAttributes()['deposit_id'];

        var value = transaction.get('value');

        if (old_deposit_id) {
            var old_deposit = window.collections.deposits.id(old_deposit_id);
            var old_deposit_currency = window.collections.currencies.id(old_deposit.get('currency_id'));
            value = value / old_deposit_currency.get('denominator');
        }

        if (deposit_id) {
            var deposit = window.collections.deposits.id(deposit_id);
            var deposit_currency = window.collections.currencies.id(deposit.get('currency_id'));
            value = Math.round(value * deposit_currency.get('denominator'));
        }

        transaction.set('value', value);
    },

    render: function(loading) {

        var form = this;

        if (loading === true) {
            this.$el.html($('<div class="loader"></div>'));

            var update_form = function() {
                this.render();
            };
            this.model.once('change', update_form, this);

            return this;
        }

        var data = this.model.toJSON();
        if (this.model.isNew()) {
            data['id'] = false;
        }

        var html = $(this.template(data));

        if (!window.collections.deposits.models.length) {
            window.collections.deposits.once('sync', function(){
                this.setDeposits(html);
            }, this);
        } else {
            this.setDeposits(html);
        }

        
        var datetime_picker = html.find('.input-group.date'),
            datetime_input = html.find('[name=date]');
        datetime_picker.datetimepicker(
        {
            language: 'en-EN',
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: datetime_input.val() || moment(),
            orientation: 'left'
        });

        datetime_picker.datetimepicker()
        .on('change', function(e) {
            form.model.set('date', form.$el.find('[name=date]').val());
        });

        this.undelegateEvents();
        this.$el.html(html);
        this.delegateEvents();

        this.stopListening(this.model, 'change:deposit_id', this.updateValueOnDepositChange);
        this.listenTo(this.model, 'change:deposit_id', this.updateValueOnDepositChange, this);

        return this;
    }
});