/**
 * Created by winzo on 05.04.14.
 */

var TransactionList = Backbone.View.extend({
    tagName: 'div',
    className: 'transaction-list loading',

    events: {
        'click div[role=page_limits] a': 'eventSetPageLimit',
        'click a[role=filter]': 'eventSetFilter',
        'click a[role=filter_deposit]': 'eventSetFilterDeposit',
        'click a[role=filter_deposit_current]': 'eventSetFilterDepositReset'
    },

    initialize: function(options)
    {
        if (options) {
            this.template = options['template'] || _.template($('#template-transaction-list-item').html());
            this.model = options['model'] || new TransactionCollection();
        } else {
            this.template = _.template($('#template-transaction-list-item').html());
            this.model = new TransactionCollection();
        }

        this.listenTo(this.model, 'sync', this.render);
    },

    eventSetPageLimit: function(event)
    {
        var $this = $(event.target);

        if ($this.hasClass('active')) return;

        this.loading(true);

        this.model.setPageSize( parseInt($this.attr('data-page_limit')) );
        navigate('transactions/page'+this.model.state.currentPage, {trigger: false, replace: true});

        if (this.$el) {
            this.$el.find('div[role=page_limits] a').removeClass('active');
            $this.addClass('active');
        }
        return false;
    },

    eventSetFilter: function(event)
    {
        var $this = $(event.target);

        if ($this.hasClass('active')) return false;

        this.loading(true);

        var filter = $this.attr('data-filter');
        var value = $this.attr('data-value');

        if (value === "") {
            delete this.model.queryParams[filter];
        } else {
            this.model.queryParams[filter] = value;
        }

        this.model.fetch();

        this.$el.find('[role=filter][data-filter='+filter+']').removeClass('active');
        $this.addClass('active');
        return false;
    },

    eventSetFilterDeposit: function(event)
    {
        var $this = $(event.target);

        this.loading(true);

        var id = $this.attr('data-id');

        if (typeof id === typeof undefined || id === false) {
            var parent = $this.closest('[data-id]');
            id = parent.attr('data-id');
        }

        var btn = this.$el.find('[role=filter_deposit_current]');
        var dropdown = this.$el.find('[role=filter_deposit_current_dropdown]');

        if (!id) {
            delete this.model.queryParams['deposit_id'];

            btn.html('All deposits');
            btn.addClass('btn-default');
            btn.removeClass('btn-primary');

            dropdown.addClass('btn-default');
            dropdown.removeClass('btn-primary');
        } else {
            id = parseInt(id);

            this.model.queryParams['deposit_id'] = id;

            btn.html(window.collections.deposits.id(id).__html__());
            btn.addClass('btn-primary');
            btn.removeClass('btn-default');

            dropdown.addClass('btn-primary');
            dropdown.removeClass('btn-default');
        }

        this.model.fetch();
        return false;
    },

    eventSetFilterDepositReset: function(event)
    {
        var $this = $(event.target);

        if ($this.hasClass('btn-default')) return false;

        this.loading(true);

        delete this.model.queryParams['deposit_id'];

        var btn = this.$el.find('[role=filter_deposit_current]');
        var dropdown = this.$el.find('[role=filter_deposit_current_dropdown]');

        btn.html('All deposits');
        btn.addClass('btn-default');
        btn.removeClass('btn-primary');

        dropdown.addClass('btn-default');
        dropdown.removeClass('btn-primary');

        this.model.fetch();
        return false;
    },

    loading: function(loading) {
        if (loading) {
            this.$el.addClass('loading');
        } else {
            this.$el.removeClass('loading');
        }
    },

    render: function()
    {
        var denominators = {};
        for(var i = 0; i < window.collections.deposits.models.length; i++) {
            var deposit = window.collections.deposits.models[i];

            var denominator = window.collections.currencies.id(deposit.get('currency_id')).get('denominator');
            denominators[deposit.get('id')] = {
                number: denominator,
                count: denominator.toString().length - 1
            };
        }

        var data = {
            model: this.model,
            results: this.model.models,
            totals: {
                profit: this.model.state.totalProfit,
                outlay: this.model.state.totalOutlay,
                income: this.model.state.totalIncome
            },
            deposits: window.collections.deposits,
            currencies: window.collections.currencies,
            denominators: denominators
        };


        if (this.model.models.length) {
            var deposit = window.collections.deposits.id(this.model.models[0].get('deposit_id'));
            var currency = window.collections.currencies.id(deposit.get('currency_id'));

            data.totals.profit = (data.totals.profit / currency.get('denominator')).toFixed(denominators[deposit.get('id')].count);
            data.totals.outlay = (data.totals.outlay / currency.get('denominator')).toFixed(denominators[deposit.get('id')].count);
            data.totals.income = (data.totals.income / currency.get('denominator')).toFixed(denominators[deposit.get('id')].count);
        }

        this.undelegateEvents();
        this.$el.html(this.template(data));
        this.delegateEvents();

        var list = this;
        setTimeout(function(){
            list.loading(false);
        }, 10);

        return this;
    }
});