/**
 * Created by winzo on 05.04.14.
 */

var TransactionCollection = Backbone.PageableCollection.extend({
    model: Transaction,
    url: '/api/transactions',

    // Any `state` or `queryParam` you override in a subclass will be merged with
    // the defaults in `Backbone.PageableCollection` 's prototype.
    state: {

        // You can use 0-based or 1-based indices, the default is 1-based.
        // You can set to 0-based by setting ``firstPage`` to 0.
        firstPage: 1,

        // Required under server-mode
        //totalRecords: 10
        pageSize: 10
    },

    // You can configure the mapping from a `Backbone.PageableCollection#state`
    // key to the query string parameters accepted by your server API.
    queryParams: {
        totalPages: null,
        totalRecords: null,

        // `Backbone.PageableCollection#queryParams` converts to ruby's
        // will_paginate keys by default.
        currentPage: "page",
        pageSize: "page_size"
    },

    parseState: function (resp, queryParams, state, options) {
        return {
            totalRecords: resp.total_count,
            totalProfit: resp.total_profit,
            totalOutlay: resp.total_outlay,
            totalIncome: resp.total_income
        };
    },

    parseRecords: function (resp, options) {
        return resp.items;
    }
});