/**
 * Created by winzo on 05.04.14.
 */

var Transaction = Backbone.Model.extend({
    urlRoot: '/api/transactions',
    idAttribute: 'id',
    defaults: function() {
        return {
            deposit_id: 0,
            value: 0,
            description: '',
            date: ''
            //creation_date: '',
            //last_modified: '',
            //user_id: 0
        };
    },
    validate: function(attrs, options) {

        if (!attrs.deposit_id) {
            return 'deposit_id is empty';
        }
        if (!_.isNumber(attrs.deposit_id)) {
            return 'deposit_id is not integer';
        }
        if (parseInt(attrs.deposit_id) <= 0) {
            return 'deposit_id is invalid';
        }

        if (!attrs.value) {
            return 'value is empty';
        }
        if (!_.isNumber(attrs.value)) {
            return 'value is not integer';
        }
        if (parseInt(attrs.value) == 0) {
            return 'value is invalid';
        }
    }
});


/*Transaction.on("invalid", function(model, error) {
  console.error("Invalid Transaction #" + model.get("id") + ": " + error);
});*/