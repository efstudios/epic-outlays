/**
 * Created by winzo on 26.04.14.
 */

var SimpleList = Backbone.View.extend({

    tagName: 'div',
    template: _.template($('#template-simple-list').html()),

    initialize: function(options) {
        var list = this;

        this.options = options;

        if (!this.options['fields']) {
            console.error('fields option is required');
            return;
        }

        list.model.fetch({
            success: function() {
                list.render();
            },
            error: function(error) {
                if (error) {
                    console.log(error)
                    list.render(error);
                } else {
                    list.render('Unknown error');
                }
            }
        });

        this.listenTo(this.model, "change", this.render);
    },

    render: function(error) {

        var data = {
            error: error ? error : false,
            fields: this.options.fields,
            results: this.model.models
        };

        this.$el.html(this.template(data));

        return this;
    }
});