/**
 * Created by winzo on 05.04.14.
 */

Backbone.emulateJSON = true;

Backbone.sync = function(method, model, options) {

    // Map from CRUD to HTTP for our default `Backbone.sync` implementation.
    var methodMap = {
        'create': 'POST',
        'update': 'PUT',
        'patch':  'PATCH',
        'delete': 'DELETE',
        'read':   'GET'
    };

    var type = methodMap[method];

    // Default options, unless specified.
    _.defaults(options || (options = {}), {
        emulateHTTP: Backbone.emulateHTTP,
        emulateJSON: Backbone.emulateJSON
    });

    // Default JSON-request options.
    var params = {type: type, dataType: 'json'};

    // Ensure that we have a URL.
    if (!options.url) {
        params.url = _.result(model, 'url') || urlError();
    }

    // Ensure that we have the appropriate request data.
    if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
        params.contentType = 'application/json';
        params.data = JSON.stringify(options.attrs || model.toJSON(options));
    }

    // For older servers, emulate JSON by encoding the request into an HTML-form.
    if (options.emulateJSON) {
        params.contentType = 'application/x-www-form-urlencoded';
        params.data = params.data ? options.attrs || model.toJSON(options) : {};
    }

    // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
    // And an `X-HTTP-Method-Override` header.
    if (options.emulateHTTP && (type === 'PUT' || type === 'DELETE' || type === 'PATCH')) {
        params.type = 'POST';
        if (options.emulateJSON) params.data._method = type;
        var beforeSend = options.beforeSend;
        options.beforeSend = function(xhr) {
            xhr.setRequestHeader('X-HTTP-Method-Override', type);
            if (beforeSend) return beforeSend.apply(this, arguments);
        };
    }

    // Don't process data on a non-GET request.
    if (params.type !== 'GET' && !options.emulateJSON) {
        params.processData = false;
    }

    // If we're sending a `PATCH` request, and we're in an old Internet Explorer
    // that still has ActiveX enabled by default, override jQuery to use that
    // for XHR instead. Remove this line when jQuery supports `PATCH` on IE8.
    if (params.type === 'PATCH' && noXhrPatch) {
        params.xhr = function() {
            return new ActiveXObject("Microsoft.XMLHTTP");
        };
    }


    // winzo
    var options_success = options.success;
    var options_error = options.error;
    options.success = function(resp, textStatus)
    {
        if (resp.status.toLowerCase() == 'done') {
            options_success(resp.data, textStatus);
        } else {
            options_error(resp.error);
        }
    };
    options.error = function (req_obj, msg, error) {
        console.error("Request error: \n" + method +' ' + params.url +"\n"+ error);
        options_error(req_obj, msg, error);
    };
    // \\winzo


    // Make the request, allowing the user to override any Ajax options.
    var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
    model.trigger('request', model, xhr, options);
    return xhr;
};

/*Backbone.sync = function(method, model, options) {

    var data = options.data;
    var request_method = 'GET';

    switch (method) {
        case 'create':
            request_method = 'POST';
            data = (model instanceof Backbone.Model) ? model.toJSON() : data;
            break;
        case 'update':
            request_method = 'PUT';
            data = (model instanceof Backbone.Model) ? model.toJSON() : data;
            break;
        case 'delete':
            request_method = 'DELETE';
            break;
        case 'read':
            request_method = 'GET';
            break;
    }

    // Прерываем старый запрос
    if (model.loading && model.loading.method == method) {
        model.loading.xhr.abort();
    }

    var request_url = _.isFunction(model.url) ? model.url() : model.url;
    var xhr = $.ajax({
        type: request_method,
        url: request_url,
        dataType: 'json',
        data: data,
        success: function(resp)
        {
            if (resp.status.toLowerCase() == 'done') {
                options.success(resp.data);
            } else {
                options.error(resp.error);
            }
        },
        error: function (req_obj, msg, error) {
            console.error("Ошибка запроса: \n" + request_method +' ' + request_url +"\n"+ error);
            options.error(error);
        }
    });

    // Сохраняем ссылку на запрос в модель
    model.loading = {
        method: method,
        xhr: xhr
    }
};*/