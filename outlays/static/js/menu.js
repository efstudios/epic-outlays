/**
 * Created by winzo on 04.04.14.
 */

var OutlaysMenu = Backbone.View.extend({

    initialized: false,

    initialize: function() {
        //this.listenTo(this.model, "change", this.render);
    },

    hide: function() {
        this.$el.addClass('js-hidden');
    },

    show: function() {
        if (this.initialized) {
            this.$el.removeClass('js-hidden');
        } else {
            var menu = this;
            setTimeout(function() {
                menu.$el.removeClass('js-hidden');
            }, 250);
        }
    },

    select: function(url) {
        this.$el.find('a.js-nav-item').removeClass('active');
        this.$el.find('a.js-nav-item[href="#'+ url +'"]').addClass('active');
        this.show();
    },

    render: function() {
        return this;
    }

});