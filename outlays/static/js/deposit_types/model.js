/**
 * Created by winzo on 07.03.15.
 */

var DepositType = Backbone.Model.extend({
    urlRoot: '/api/deposits_types',
    idAttribute: 'id',
    defaults: function() {
        return {
            "name": "",
            "description": ""
        };
    },
    validate: function(attrs, options) {

        if (!attrs.name) {
            return 'name is empty';
        }
        if (!attrs.description) {
            return 'description is empty';
        }
    },
    __str__: function() {
        return this.get('name');
    }
});