/**
 * Created by winzo on 20.03.14.
 */

var Currency = Backbone.Model.extend({
    urlRoot: '/api/currencies',
    idAttribute: 'id',
    defaults: function() {
        return {
            code: "",
            description: "",
            denominator: 1,
            sign: ""
        };
    },
    validate: function(attrs, options) {

        if (!attrs.code) {
            return 'code is empty';
        }

        if (!attrs.sign) {
            return 'sign is empty';
        }

        if (!attrs.denominator) {
            return 'denominator is empty';
        }
        if (!_.isNumber(attrs.denominator)) {
            return 'denominator is not integer';
        }
        if (parseInt(attrs.denominator) <= 0) {
            return 'denominator is invalid';
        }
    },
    __str__: function() {
        return this.get('sign') + ", " + this.get('description') + " (" + this.get('code') + ")";
    }
});


/*Currency.on("invalid", function(model, error) {
  console.error("Invalid currency #" + model.get("id") + ": " + error);
});*/