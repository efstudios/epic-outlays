/**
 * Created by winzo on 04.04.14.
 */

var OutlaysHeader = Backbone.View.extend({

    initialize: function() {
        var header = this;
        if(header.$el.find('.js-title').html()) {
            setTimeout(function() {
                header.$el.removeClass('js-hidden');
            }, 250);
        }
    },

    change: function(title, description) {
        var header = this;
        var title_div = this.$el.find('.js-title');

        if (title_div.html() != title) {
            this.$el.addClass('js-hidden');

            setTimeout(function(){
                title_div.html(title);
                header.$el.find('.js-description').html(description);
                header.$el.removeClass('js-hidden');
            }, 250);
        } else {
            header.description(description);
        }
    },

    description: function(description) {
        var description_div = this.$el.find('.js-description');

        if (description_div.html() != description) {
            description_div.addClass('js-hidden');

            setTimeout(function(){
                description_div.html(description);
                description_div.removeClass('js-hidden');
            }, 250);
        }
    },

    render: function() {
        return this;
    }

});