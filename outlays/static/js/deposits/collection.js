/**
 * Created by winzo on 05.04.14.
 */

var DepositCollection = Backbone.PageableCollection.extend({
    model: Deposit,
    url: '/api/deposits',

    // Any `state` or `queryParam` you override in a subclass will be merged with
    // the defaults in `Backbone.PageableCollection` 's prototype.
    state: {

        // You can use 0-based or 1-based indices, the default is 1-based.
        // You can set to 0-based by setting ``firstPage`` to 0.
        firstPage: 1,

        // Required under server-mode
        totalRecords: 100
    },

    // You can configure the mapping from a `Backbone.PageableCollection#state`
    // key to the query string parameters accepted by your server API.
    queryParams: {

        // `Backbone.PageableCollection#queryParams` converts to ruby's
        // will_paginate keys by default.
        currentPage: "page",
        pageSize: "page_size"
    },

    parseState: function (resp, queryParams, state, options) {
        return {totalRecords: resp.total_count};
    },

    parseRecords: function (resp, options) {
        return resp.items;
    },
    id: function(id, options) {
        id = parseInt(id);
        var data = {};
        data[this.model.idAttribute || 'id'] = id;

        var result = this.findWhere(data);

        if (!result && options) {
            result = new Deposit({id: id});
            result.fetch(options);
        }

        return result;
    }
});