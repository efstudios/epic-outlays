/**
 * Created by winzo on 05.04.14.
 */

var Deposit = Backbone.Model.extend({
    urlRoot: '/api/deposits',
    idAttribute: 'id',
    defaults: function() {
        return {
            currency_id: 0,
            type_id: 0,
            value: 0,
            value_min: null,
            value_max: null,
            description: '',
            date: ''
        };
    },
    validate: function(attrs, options) {

        if (!attrs.description) {
            return 'description is empty';
        }

        if (!attrs.type_id) {
            return 'type_id is empty';
        }
        if (!_.isNumber(attrs.type_id)) {
            return 'type_id is not integer';
        }
        if (parseInt(attrs.type_id) <= 0) {
            return 'type_id is invalid';
        }

        if (!_.isNumber(attrs.value)) {
            return 'value is not integer';
        }
        if (attrs.value_min != null && !_.isNumber(attrs.value_min)) {
            return 'value_min is not integer';
        }
        if (attrs.value_max != null && !_.isNumber(attrs.value_max)) {
            return 'value_max is not integer';
        }
    },
    __str__: function() {
        return this.get('description');
    },
    __html__: function() {

        var currency = window.collections.currencies.id(this.get('currency_id'));

        var icon = '';
        switch (this.get('type_id')) {
            case 1:
                icon = '<span class="glyphicon glyphicon-briefcase"></span>';
                break;
            case 2:
                icon = '<span class="glyphicon glyphicon-credit-card text-success"></span>';
                break;
            case 3:
                icon = '<span class="glyphicon glyphicon-credit-card text-danger"></span>';
                break;
            case 4:
                icon = '<span class="glyphicon glyphicon-folder-close"></span>';
                break;
        }

        return '<span title="'+ currency.get('sign')+' '+this.get('value')/currency.get('denominator') +'">'+ icon +'&nbsp;'+ this.get('description') +'</span>';
    }
});


/*Deposit.on("invalid", function(model, error) {
  console.error("Invalid Deposit #" + model.get("id") + ": " + error);
});*/