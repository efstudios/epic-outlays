/**
 * Created by winzo on 05.04.14.
 */

var DepositList = Backbone.View.extend({
    tagName: 'div',
    className: 'deposit-list loading',

    events: {
        'click div[role=page_limits] a': 'eventSetPageLimit'
    },

    initialize: function(options)
    {
        if (options) {
            this.template = options['template'] || _.template($('#template-deposit-list-item').html());
            this.model = options['model'] || new DepositCollection();
        } else {
            this.template = _.template($('#template-deposit-list-item').html());
            this.model = new DepositCollection();
        }

        this.listenTo(this.model, 'sync', this.render);
        //this.listenTo(this.model, 'change:page_limit', this.updatePage);
        //this.listenTo(this.model, 'change:page_limits', this.render);

        //this.listenTo(this.collection, 'sync', this.render);
    },

    eventSetPageLimit: function(event)
    {
        var $this = $(event.target);

        if ($this.hasClass('active')) return;

        this.loading(true);

        this.model.setPageSize( parseInt($this.attr('data-page_limit')) );
        navigate('deposits/page'+this.model.state.currentPage, {trigger: false, replace: true});

        if (this.$el) {
            this.$el.find('div[role=page_limits] a').removeClass('active');
            $this.addClass('active');

            /*this.$el.find('div[role=page_limits] a').removeClass('btn-primary');
            this.$el.find('div[role=page_limits] a').addClass('btn-default');

            var current = this.$el.find('div[role=page_limits] a[data-page_limit='+ this.model.get('page_limit') +']');
            current.removeClass('btn-default');
            current.addClass('btn-primary');*/
        }
        return false;
    },

    loading: function(loading) {
        if (loading) {
            this.$el.addClass('loading');
        } else {
            this.$el.removeClass('loading');
        }
    },

    render: function()
    {
        var denominators = {};
        for(var i = 0; i < this.model.models.length; i++) {
            var deposit = this.model.models[i];

            var denominator = window.collections.currencies.id(deposit.get('currency_id')).get('denominator');
            denominators[deposit.get('id')] = {
                number: denominator,
                count: denominator.toString().length - 1
            };
        }

        var data = {
            model: this.model,
            results: this.model.models,
            deposits: window.collections.deposits,
            currencies: window.collections.currencies,
            denominators: denominators
        };

        this.undelegateEvents();
        this.$el.html(this.template(data));
        this.delegateEvents();

        this.loading(false);

        return this;
    }
});