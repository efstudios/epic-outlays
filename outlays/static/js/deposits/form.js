/**
 * Created by winzo on 07.03.15.
 */


var DepositForm = Backbone.View.extend({

    tagName: 'form',
    className: 'form-horizontal',
    template: _.template($('#template-deposit-form').html()),

    events: {
        'change [name=currency_id]': function() {
            this.model.set('currency_id', parseInt(this.$el.find('[name=currency_id]').val()))
        },
        'change [name=type_id]': function() {
            this.model.set('type_id', parseInt(this.$el.find('[name=type_id]').val()))
        },
        'change [name=value_max]': function() {
            var text_val = this.$el.find('[name=value_max]').val();
            if (text_val == "") {
                this.model.set('value_max', null);
                return;
            }

            var value_max = parseFloat(text_val);

            if (this.model.get('currency_id')) {
                var currency = window.collections.currencies.id(this.model.get('currency_id'));
                value_max = Math.round(value_max * currency.get('denominator'));
            }

            this.model.set('value_max', value_max);
        },
        'change [name=value]': function() {
            var value = parseFloat(this.$el.find('[name=value]').val());

            if (this.model.get('currency_id')) {
                var currency = window.collections.currencies.id(this.model.get('currency_id'));
                value = Math.round(value * currency.get('denominator'));
            }

            this.model.set('value', value);
        },
        'change [name=value_min]': function() {
            var text_val = this.$el.find('[name=value_min]').val();
            if (text_val == "") {
                this.model.set('value_min', null);
                return;
            }

            var value_min = parseFloat(text_val);

            if (this.model.get('currency_id')) {
                var currency = window.collections.currencies.id(this.model.get('currency_id'));
                value_min = Math.round(value_min * currency.get('denominator'));
            }

            this.model.set('value_min', value_min);
        },
        'change [name=description]': function() {
            this.model.set('description', this.$el.find('[name=description]').val())
        },
        'change [name=date]': function() {
            this.model.set('date', this.$el.find('[name=date]').val())
        },
        'click [role=delete]': 'remove',
        'click [role=save]': 'save'
    },

    initialize: function() {
        var currency_id = null;
        if (this.model.get('currency_id')) {
            currency_id = this.model.get('currency_id');
        }

        this.listenTo(this.model, 'invalid', function(model, error) {
            console.error(error);
            alert(error);
        });

        this.listenTo(this.model, 'sync', function()
        {
            if (currency_id) {
                window.collections.currencies.id(currency_id).fetch();
            }
            if (this.model.get('currency_id')) {
                window.collections.currencies.id(this.model.get('currency_id')).fetch();
            }

            this.render();
        }, this);
    },

    save: function(event) {
        var btn = $(event.target);//this.$el.find('[role=save]');
        var error_text = this.$el.find('[role=save_error]');

        if (btn.hasClass('disabled')) {
            return;
        }

        btn.addClass('loading disabled');
        error_text.html('');

        this.model.once('invalid', function(){
            btn.removeClass('loading');
            btn.removeClass('disabled');
        }, this);

        var data = {};
        var date_input = this.$el.find('[name=date]').val();
        if (date_input && !this.model.get('date')) {
            data['date'] = date_input;
        }

        var model = this.model;
        this.model.save(data, {
            success: function() {
                btn.removeClass('loading');
                btn.removeClass('disabled');

                var exists = window.collections.deposits.id(model.get('id'));
                if (!exists) {
                    window.collections.deposits.add(model);
                }

                navigate('deposits', true);
            },
            error: function(model, error) {
                btn.removeClass('loading');
                btn.removeClass('disabled');
                error_text.html(error.desc);
            }
        });
    },

    remove: function(event) {
        var btn = $(event.target);
        var error_text = this.$el.find('[role=save_error]');

        if (btn.hasClass('disabled')) {
            return;
        }

        btn.addClass('loading disabled');
        error_text.html('');

        this.model.destroy({
            success: function() {
                btn.removeClass('loading');
                btn.removeClass('disabled');
                navigate('deposits', true);
            },
            error: function(model, error) {
                btn.removeClass('loading');
                btn.removeClass('disabled');
                error_text.html(error.desc);
            }
        });
    },

    setCurrencies: function(html) {
        var currency = html.find('[name=currency_id]');
        currency.html('');

        var option = $('<option value=""></option>');
        currency.append(option);

        for (var i = 0; i < window.collections.currencies.models.length; i++) {
            var model = window.collections.currencies.models[i];

            option = $('<option value="'+ model.get('id') +'">'+ model.__str__() +'</option>');

            if (model.get('id') == this.model.get('currency_id')) {
                option.attr('selected', '');
            }

            currency.append(option);
        }
    },

    setDepositTypes: function(html) {
        var deposit_type = html.find('[name=type_id]');
        deposit_type.html('');

        var option = $('<option value=""></option>');
        deposit_type.append(option);

        for (var i = 0; i < window.collections.deposit_types.models.length; i++) {
            var model = window.collections.deposit_types.models[i];

            option = $('<option value="'+ model.get('id') +'">'+ model.__str__() +'</option>');

            if (model.get('id') == this.model.get('type_id')) {
                option.attr('selected', '');
            }

            deposit_type.append(option);
        }
    },

    updateValueOnCurrencyChange: function(deposit, currency_id)
    {
        var old_currency_id = deposit.previousAttributes()['currency_id'];

        var value_max = deposit.get('value_max');
        var value_min = deposit.get('value_min');

        if (old_currency_id) {
            var old_currency = window.collections.currencies.id(old_currency_id);
            if (value_max !== null) value_max = value_max / old_currency.get('denominator');
            if (value_min !== null) value_min = value_min / old_currency.get('denominator');
        }

        if (currency_id) {
            var currency = window.collections.currencies.id(currency_id);
            if (value_max !== null) value_max = Math.round(value_max * currency.get('denominator'));
            if (value_min !== null) value_min = Math.round(value_min * currency.get('denominator'));
        }

        deposit.set('value_max', value_max);
        deposit.set('value_min', value_min);
    },

    render: function(loading) {

        var form = this;

        if (loading === true) {
            this.$el.html($('<div class="loader"></div>'));

            var update_form = function() {
                this.render();
            };
            this.model.once('change', update_form, this);

            return this;
        }

        var data = this.model.toJSON();
        if (this.model.isNew()) {
            data['id'] = false;
        }

        var html = $(this.template(data));

        if (!window.collections.currencies.models.length) {
            window.collections.currencies.once('sync', function(){
                this.setCurrencies(html);
            }, this);
        } else {
            this.setCurrencies(html);
        }

        if (!window.collections.deposit_types.models.length) {
            window.collections.deposit_types.once('sync', function(){
                this.setDepositTypes(html);
            }, this);
        } else {
            this.setDepositTypes(html);
        }


        var datetime_picker = html.find('.input-group.date'),
            datetime_input = html.find('[name=date]');
        datetime_picker.datetimepicker(
        {
            language: 'en-EN',
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: datetime_input.val() || moment(),
            orientation: 'left'
        });

        datetime_picker.datetimepicker()
        .on('change', function(e) {
            form.model.set('date', form.$el.find('[name=date]').val());
        });

        this.undelegateEvents();
        this.$el.html(html);
        this.delegateEvents();

        this.stopListening(this.model, 'change:currency_id', this.updateValueOnCurrencyChange);
        this.listenTo(this.model, 'change:currency_id', this.updateValueOnCurrencyChange, this);

        return this;
    }
});