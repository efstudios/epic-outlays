
var body_toggle = function() {
    var args = arguments;
    var body = $('.js-body');
    body.addClass('js-hidden');
    setTimeout(function(){
        body.empty();
        for (var i in args) {
            body.append(args[i]);
        }
        body.removeClass('js-hidden');
    }, 250);
};

var Router = Backbone.Router.extend({

    prefix: '',

    options: {
        views: {},
        history: []
    },

    routes: {
        '': 'transactions',
        'transactions/new': 'transaction_new',
        'transactions': 'transactions',
        'transactions/page:page': 'transactions',
        'transactions/:id/delete': 'transaction_delete',
        'transactions/:id': 'transaction_edit',
        'deposits/new': 'deposit_new',
        'deposits': 'deposits',
        'deposits/page:page': 'deposits',
        'deposits/:id/delete': 'deposit_delete',
        'deposits/:id': 'deposit_edit',
        'currencies': 'currencies',
        '.*': 'page404'
    },

    transactions: function(page)
    {
        window.menu.select('transactions');
        window.header.change('Transactions', 'Your transactions');

        if (!this.options.views.transactions) {
            this.options.views.transactions = new TransactionList();

            this.options.views.transactions.model.getFirstPage();
        } else {
            this.options.views.transactions.model.getPage(parseInt(page) || this.options.views.transactions.model.state.firstPage);
        }

        //this.options.views.transactions.$el.html('');
        this.options.views.transactions.loading(true);
        this.body_toggle([this.options.views.transactions.el], 'transactions/page', 'transactions');
    },

    transaction_new: function()
    {
        window.menu.select('transactions');
        window.header.change('Transactions', 'Create new transaction');

        var model = new Transaction();
        var view = new TransactionForm({
            model: model,
            template: _.template($('#template-transaction-form').html())
        });

        this.body_toggle([view.render().el]);
    },

    transaction_edit: function(id)
    {
        window.menu.select('transactions');
        window.header.change('Transactions', 'Edit transaction #'+id);

        var router = this;

        var loading = true;

        var model = new Transaction({id: id});
        model.fetch({
            error: function (model, error, err) {
                switch(error.kind) {
                    case 'DOES_NOT_EXIST':
                        router.page404();
                        break;
                    case 'ACCESS_DENIED':
                        router.page404();
                        break;
                }
            }
        });

        var view = new TransactionForm({
            model: model,
            template: _.template($('#template-transaction-form').html())
        });

        view.render(loading);

        this.body_toggle([view.el]);
    },

    transaction_delete: function(id)
    {
        window.menu.select('transactions');
        window.header.change('Transactions', 'Deleting transaction #'+id);

        var router = this;

        var model = new Transaction({id: id});
        model.fetch({
            error: function (model, error, err) {
                switch(error.kind) {
                    case 'DOES_NOT_EXIST':
                        router.page404();
                        break;
                    case 'ACCESS_DENIED':
                        router.page404();
                        break;
                }
            },
            success: function() {
                model.destroy({
                    success: function() {
                        window.history.back();
                    },
                    error: function(model, error) {
                        body_toggle(error.desc);
                    }
                });
            }
        });

        this.body_toggle(['<center><img src="/static/img/loader.gif"></center>']);
    },

    deposits: function(page)
    {
        window.menu.select('deposits');
        window.header.change('Deposits', 'Your deposits');

        if (!this.options.views.deposits) {
            this.options.views.deposits = new DepositList();

            this.options.views.deposits.model.getFirstPage();
        } else {
            this.options.views.deposits.model.getPage(parseInt(page) || this.options.views.deposits.model.state.firstPage);
        }

        //this.options.views.deposits.$el.html('');
        this.options.views.deposits.loading(true);
        this.body_toggle([this.options.views.deposits.el], 'deposits/page', 'deposits');
    },

    deposit_new: function()
    {
        window.menu.select('deposits');
        window.header.change('Deposits', 'Create new deposit');

        var model = new Deposit();
        var view = new DepositForm({
            model: model,
            template: _.template($('#template-deposit-form').html())
        });

        this.body_toggle([view.render().el]);
    },

    deposit_edit: function(id)
    {
        window.menu.select('deposits');
        window.header.change('Deposits', 'Edit deposit #'+id);

        var router = this;

        var model = window.collections.deposits.id(id, {
            error: function (model, error, err) {
                switch(error.kind) {
                    case 'DOES_NOT_EXIST':
                        router.page404();
                        break;
                    case 'ACCESS_DENIED':
                        router.page404();
                        break;
                }
            }
        });
        var loading = !model;

        var view = new DepositForm({
            model: model,
            template: _.template($('#template-deposit-form').html())
        });

        view.render(loading);

        this.body_toggle([view.el]);
    },

    deposit_delete: function(id)
    {
        window.menu.select('deposits');
        window.header.change('Deposits', 'Deleting deposit #'+id);

        var router = this;

        var error = false;
        var model = window.collections.deposits.id(id, {
            error: function (model, error, err) {
                switch(error.kind) {
                    case 'DOES_NOT_EXIST':
                        error = true;
                        router.page404();
                        break;
                    case 'ACCESS_DENIED':
                        error = true;
                        router.page404();
                        break;
                }
            }
        });
        var id = model.get('id');
        model.destroy({
            success: function() {
                var exists = window.collections.deposits.id(id);
                if (exists) {
                    window.collections.deposits.remove(model);
                }
                window.history.back();
            },
            error: function(model, error) {
                if (!error) {
                    body_toggle(error.desc);
                }
            }
        });

        this.body_toggle(['<center><img src="/static/img/loader.gif"></center>']);
    },

    currencies: function(page)
    {
        window.menu.select('currencies');
        window.header.change('Currencies', 'Your and global currencies');

        var view = new SimpleList({
            model: window.collections.currencies,
            template: _.template($('#template-simple-list').html()),
            fields: ['code','sign','description']
        });

        var button_new_currency = $('<a href="#currencies/new">New</a>');

        body_toggle(button_new_currency, view.render().el);
    },

    page404: function() {
        window.menu.select('404');
        window.header.change('404', 'Page not found');

        this.body_toggle(['lol']);
    },

    body_toggle: function(args, start, full) {
        //var args = arguments;
        //delete args[0];
        var body = $('.js-body');

        // if previous url starts with start string
        // or equals to full string then don't change body
        if (this.options.history.length > 0 && start
            ? (this.options.history[this.options.history.length-1].fragment.indexOf(start) !== 0) &&
            ( this.options.history.length > 0 && full
                ? this.options.history[this.options.history.length-1].fragment != full
                : true
            ) : true) {

            body.addClass('js-hidden');
            setTimeout(function(){
                body.empty();
                for (var i in args) {
                    body.append(args[i]);
                }
                body.removeClass('js-hidden');
            }, 250);

        } else {
            body.removeClass('js-hidden');
        }

    },

    initialize: function(options)
    {
        $.ajaxSetup({ cache: false });

        if (!window.collections) {
            window.collections = {};
        }
        if (!window.collections.deposits) {
            window.collections.deposits = new DepositCollection();
            window.collections.deposits.fetch();
        }
        if (!window.collections.currencies) {
            window.collections.currencies = new CurrencyCollection();
            window.collections.currencies.fetch();
        }
        if (!window.collections.deposit_types) {
            window.collections.deposit_types = new DepositTypeCollection();
            window.collections.deposit_types.fetch();
        }

        this.listenTo(this, 'route', function (name, args) {
            this.options.history.push({
                name : name,
                args : args,
                fragment : Backbone.history.fragment
            });
        });

        window.navigate = this.navigate;
        Backbone.history.start();
    }
});