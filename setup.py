#!/usr/bin/env python2
# -*- coding: utf-8 -*-
__author__ = 'winzo'


from distutils.core import setup

setup(name='epic-outlays',
      version='0.0',
      description='Epic Outlays',
      packages=['epic-outlays'],
      author='EpicFail Studios',
      author_email='dev@efstudios.org',
      url='http://outlays.efstudios.org/',
      license='The MIT License (MIT)',
     )