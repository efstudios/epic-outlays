import React from 'react'


export const CurrenciesContext = React.createContext(null)
export const CurrencyContext = React.createContext(null)

export const DepositsContext = React.createContext(null)
export const DepositContext = React.createContext(null)

export const DepositTypesContext = React.createContext(null)

export function find(list, id) {
  for (let item of list) {
    if (item.id === id) return item
  }
  return null
}

export function update(list, item) {
  for (let i = 0; i < list.length; i++) {
    if (item.id === list[i].id) {
      list[i] = { ...list[i], ...item }
      break
    }
  }
  return [...list]
}