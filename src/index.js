import React from 'react'
import ReactDOM from 'react-dom'
import { App } from './components/app'

ReactDOM.render(
  <App currentUser={window.eo.currentUser}
    currencies={window.eo.currencies}
    deposits={window.eo.deposits}
    transactions={window.eo.transactions}
    deposit_types={window.eo.deposit_types} />,
  document.getElementById('root')
)
