import React, { useState, useEffect } from 'react'

import { apiFetch } from '../api'
import { fadeInDownAnimationFast } from '../animation'

function get(obj, key) {
  if (key.includes('.')) {
    const keys = key.split('.')
    return get(obj[keys[0]], keys.slice(1))
  }
  return obj[key]
}

function groupBy(xs, key) {
  return xs.reduce(function (rv, x) {
    (rv[get(x, key)] = rv[get(x, key)] || []).push(x)
    return rv
  }, {})
}

export default function List(props) {
  const [state, setState] = useState({
    loading: !props.items || !props.items.length,
    items: props.items
  })
  console.log('List', state)

  useEffect(() => {
    async function fetchData() {
      try {
        const resJson = await apiFetch(props.location.pathname)
        console.log('resJson', resJson)
        setState({
          loading: false,
          items: resJson.items
        })
      } catch (e) {
        console.log('e', e)
        setState({
          loading: false,
          errors: e.errors ? e.errors : [{ title: e.message, status: e.name }],
          items: null
        })
      }
    }
    fetchData() // useEffect function can not be async
  }, [])

  if (state.loading) {
    return (
      <ul className={'list-group ' + fadeInDownAnimationFast + (props.className ? ' ' + props.className : '')}>
        <li className='list-group-item text-center'>
          <div className=' spinner-border text-dark' role='status'>
            <span className='sr-only'>Loading...</span>
          </div>
        </li>
      </ul>
    )
  }

  if (state.errors) {
    return (
      <ul className={'list-group ' + fadeInDownAnimationFast + (props.className ? ' ' + props.className : '')}>
        {state.errors.map((error) =>
          <li className='list-group-item text-danger' key={error.status}>{error.title}</li>
        )}
      </ul>
    )
  }

  if (!state.items) {
    return (
      <ul className={'list-group ' + fadeInDownAnimationFast + (props.className ? ' ' + props.className : '')}>
        <li className='list-group-item text-muted'>Empty list</li>
      </ul>
    )
  }

  if (!props.groupBy) {
    return (
      <ul className={'list-group ' + fadeInDownAnimationFast + (props.className ? ' ' + props.className : '')}>
        {state.items ? state.items.map((item) =>
          <props.item key={item.id} model={item} />
        ) : ''}
      </ul>
    )
  }

  const GroupItem = (groupItemProps) => {
    if (!props.groupItem) return
    const Component = props.groupItem
    return (
      <Component {...groupItemProps} />
    )
  }

  const groups = groupBy(state.items, props.groupBy)
  return (
    <React.Fragment>
      {Object.keys(groups).map(
        (field) => (
          <ul className={'list-group ' + fadeInDownAnimationFast + (props.className ? ' ' + props.className : '')} key={field}>
            <GroupItem field={field} items={groups[field]} />
            {groups[field].map((item) =>
              <props.item key={item.id} model={item} />
            )}
          </ul>
        )
      )}
    </React.Fragment>
  )
}
