import React from 'react'

function RenderInput({ name, displayName, model, bindChange }) {
  displayName = displayName || name
  return (
    <div className='form-group' key={name}>
      <label htmlFor='{name}' className='text-capitalize'>{displayName}</label>
      <input type='text' className='form-control' name={name} onBlur={bindChange(name, (e) => e.target.value)} defaultValue={model ? model[name] : ''} />
    </div>
  )
}

export default function Input(fieldProps) {
  return (props) => {
    const allProps = { ...props, ...fieldProps }
    return (
      <RenderInput key={allProps.name} {...allProps} />
    )
  }
}
