import React, { useContext } from 'react'

import { DepositsContext, CurrenciesContext, DepositContext, CurrencyContext, find } from '../../../context'

function RenderInput({ name, displayName, model, bindChange }) {
  displayName = displayName || name
  const currency = useContext(CurrencyContext)
  const denominator = currency ? currency.denominator : 1
  return (
    <div className='form-group' key={name}>
      <label htmlFor='{name}' className='text-capitalize'>{displayName}</label>
      <input type='text' className='form-control' name={name}
        onBlur={bindChange(name, (e) => parseFloat(e.target.value) * denominator)}
        defaultValue={model ? model[name] / denominator : ''} />
    </div>
  )
}

export const NumberInputByDeposit = (fieldProps, getId) => (props) => {
  const allProps = { ...props, ...fieldProps }

  const deposits = useContext(DepositsContext)
  const deposit = find(deposits, getId(allProps))

  const currencies = useContext(CurrenciesContext)
  const currency = deposit ? find(currencies, deposit.currency_id) : null

  return (
    <DepositContext.Provider key={allProps.name} value={deposit}>
      <CurrencyContext.Provider value={currency}>
        <RenderInput {...allProps} />
      </CurrencyContext.Provider>
    </DepositContext.Provider>
  )
}

export const NumberInputByCurrency = (fieldProps, getId) => (props) => {
  const allProps = { ...props, ...fieldProps }

  const currencies = useContext(CurrenciesContext)
  const currency = find(currencies, getId(allProps))

  return (
    <CurrencyContext.Provider key={allProps.name} value={currency}>
      <RenderInput {...allProps} />
    </CurrencyContext.Provider>
  )
}

export default function NumberInput(fieldProps) {
  return (props) => {
    const allProps = { ...props, ...fieldProps }
    return (
      <RenderInput key={allProps.name} {...allProps} />
    )
  }
}
