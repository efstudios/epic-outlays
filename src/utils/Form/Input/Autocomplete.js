import React from 'react'
import { Select } from 'react-select-virtualized'

function RenderAutocomplete({ name, displayName, options, model, bindChange, renderOption }) {
  displayName = displayName || name
  return (
    <div className='form-group' key={name}>
      <label htmlFor='{name}' className='text-capitalize'>{displayName}</label>
      <Select
        onChange={bindChange(name, (option) => option.value)}
        options={options}
        value={options.find(option => option.value === model[name])}
        getOptionLabel={option => option.label}
        getOptionValue={(option) => option.value}
        formatOptionLabel={renderOption}
      />
    </div>
  )
}

export default function MyAutocomplete(fieldProps) {
  return (props) => {
    const allProps = { ...props, ...fieldProps }
    return (
      <RenderAutocomplete key={allProps.name} ss {...allProps} />
    )
  }
}
