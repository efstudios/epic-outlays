import React, { useState, useEffect } from 'react'
import { useDebouncedCallback } from 'use-debounce'

import { apiFetch, apiUpdate } from '../api'
import { Page } from '../Page'
import { fadeInDownAnimationFast } from '../animation'

export default function Form(props) {
  const [state, setState] = useState({
    loading: true, // props.location.state ? !props.location.state.model : true,
    model: null, // props.location.state ? props.location.state.model : null,
    saved: false, // props.location.state ? !!props.location.state.model : false,
    errors: null
  })

  console.log('Form', props, state)

  const update = (data) => {
    const newState = {
      ...state,
      model: {
        ...state.model,
        ...data
      }
    }

    debouncedApiUpdate(props.location.pathname, newState.model)
    setState({ ...newState, saved: false })
  }

  const [debouncedApiUpdate] = useDebouncedCallback(
    (url, data) => {
      apiUpdate(url, data).then(
        (resJson) => {
          setState({ ...state, model: { ...resJson }, saved: true })
          if (props.onSave) props.onSave(resJson)
        }
      )
    },
    500
  )

  // code to update model on show
  const modelId = state.model ? state.model.id : props.match.params.id
  useEffect(() => {
    async function fetchData() {
      try {
        const resJson = await apiFetch(props.location.pathname)
        setState({ loading: false, model: resJson, saved: true })
      } catch (e) {
        setState({ loading: false, errors: e.errors, model: null })
      }
    }
    if (!state.model) fetchData() // useEffect function can not be async
  }, [modelId])

  if (state.loading) {
    return (
      <Page title={props.title({ model: state.model, modelId })}>
        <ul className={'list-group ' + fadeInDownAnimationFast}>
          <li className='list-group-item text-center'>
            <div className='spinner-border text-dark' role='status'>
              <span className='sr-only'>Loading...</span>
            </div>
          </li>
        </ul>
      </Page>
    )
  }

  if (state.errors) {
    return (
      <Page title={props.title({ model: state.model, modelId })}>
        <div className={'list errors'}>
          {state.errors.map((error) =>
            <div className='text-danger' key={error.status}>{error.title}</div>
          )}
        </div>
      </Page>
    )
  }

  const bindChange = (field, value) => {
    return function () {
      const data = {}
      data[field] = value.apply(this, arguments)
      update(data)
    }
  }

  const Preview = props.preview
  const children = typeof props.children === 'function' ? [props.children] : props.children
  return (
    <Page title={props.title({ model: state.model, modelId })}>
      <div className='card preview'>
        <div className={'card-body' + (state.saved ? '' : ' text-white bg-primary loading')}>
          <Preview model={state.model} />
        </div>
      </div>

      <form className='form'>
        {children ? children.map((Children) => (typeof Children === 'function') ? Children({ model: state.model, bindChange }) : Children) : ''}
      </form>
    </Page>
  )
}
