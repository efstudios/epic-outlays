import { fadeInDown } from 'react-animations'
import { StyleSheet, css } from 'aphrodite'

const styles = StyleSheet.create({
  fadeInDownSlow: {
    animationName: fadeInDown,
    animationDuration: '500ms'
  },
  fadeInDown: {
    animationName: fadeInDown,
    animationDuration: '250ms'
  }
})

export const fadeInDownAnimation = css(styles.fadeInDownSlow)
export const fadeInDownAnimationFast = css(styles.fadeInDown)
