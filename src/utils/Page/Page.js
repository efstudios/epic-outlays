import React from 'react'

import { fadeInDownAnimationFast } from '../animation'

export default function Page (props) {
  const {title, ...filteredProps} = props

  // const renderTitle = (title) => {
  //   if (title) {
  //     return (<h1 className='font-weight-light'>{title}</h1>)
  //   }
  // }

  return (
    <div className={'page ' + fadeInDownAnimationFast + (filteredProps.className ? ' ' + filteredProps.className : '')} {...filteredProps}>
      {
        // renderTitle()
      }
      {filteredProps.children}
    </div>
  )
}
