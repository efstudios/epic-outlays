import React from 'react'

export function withProps(Component, props) {
  return function (matchProps) {
    return <Component {...props} {...matchProps} />
  }
}

export function onClickWrapper(onClick) {
  return function (e) {
    e.preventDefault()
    onClick(e)
    return false
  }
}

function renderTransaction(model, deposit) {
  const value = parseFloat(model.value).toFixed(2)
  return (
    <div>
      <span className='float-right text-muted'>{value + ' ' + deposit.currency_id}</span>
      {model.description}
    </div>
  )
}
