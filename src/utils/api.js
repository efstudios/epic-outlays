const apiPrefix = '/api'

const processResponse = (res) => {
  return new Promise(function (resolve, reject) {
    if (res.ok) {
      res.json().then((data) => {
        if (data.status === 'DONE') return resolve(data.data)
        reject(data)
      }).catch(reject)
    } else {
      res.json().then((data) => {
        reject(data)
      }).catch(reject)
    }
  })
}

export function apiFetch(url) {
  // return new Promise((resolve, reject) => resolve({ ok: false }))
  return fetch(apiPrefix + url).then(processResponse)
}

export function apiUpdate(url, data) {
  var formData = new FormData()
  for (var key in data) formData.append(key, data[key] ? data[key] : '')
  return fetch(apiPrefix + url, {
    method: 'PUT',
    body: formData
  }).then(processResponse)
}
