import React, { useContext } from 'react'
import { Link } from 'react-router-dom'

import { List } from '../../utils/List'
import { Page } from '../../utils/Page'
import { Transaction } from './Transaction'
import { TransactionGroup } from './TransactionGroup'
import { TransactionCreateForm } from './TransactionCreateForm'

import { DepositsContext, DepositContext, CurrenciesContext, CurrencyContext, find } from '../../context'

function TransactionItem({ model }) {
  console.log('TransactionItem', model)
  const deposits = useContext(DepositsContext)
  const deposit = find(deposits, model.deposit_id)

  const currencies = useContext(CurrenciesContext)
  const currency = deposit ? find(currencies, deposit.currency_id) : null

  return (
    <DepositContext.Provider value={deposit}>
      <CurrencyContext.Provider value={currency}>
        <Link className='list-group-item list-group-item-action' to={{ pathname: `/transactions/${model.id}`, state: { model } }}>
          <Transaction model={model} />
        </Link>
      </CurrencyContext.Provider>
    </DepositContext.Provider >
  )
}

export function TransactionList(props) {
  console.log('TransactionList', props)
  const { location, history, transactions } = props
  return (
    <Page title='Transactions'>
      <div className='list-actions'>
        <TransactionCreateForm />
      </div>
      <List item={TransactionItem} groupBy='date' groupItem={TransactionGroup} {...{ location, history, items: transactions }} />
    </Page>
  )
}
