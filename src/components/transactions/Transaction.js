import React, { useContext } from 'react'

import { CurrenciesContext, DepositsContext, find } from '../../context'


export function Transaction({ model }) {
  // todo take Currency from Context
  // const currency = useContext(CurrencyContext)

  const deposits = useContext(DepositsContext)
  const deposit = model ? find(deposits, model.deposit_id) : null

  const currencies = useContext(CurrenciesContext)
  const currency = deposit ? find(currencies, deposit.currency_id) : null

  if (!model) return (<div></div>)

  const value = parseFloat(model.value / currency.denominator).toFixed(2)
  return (
    <div>
      <span className='float-right text-muted'>{value + ' ' + currency.sign}</span>
      {model.description}
    </div>
  )
}
