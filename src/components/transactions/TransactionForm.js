import React from 'react'

import { withProps } from '../../utils/react'

import { Form } from '../../utils/Form'
import { Input, NumberInputByDeposit } from '../../utils/Form/Input'
import { Transaction } from './Transaction'

export function TransactionForm(props) {
  console.log('TransactionForm', props)
  const { location, history, match } = props

  return (
    <Form title={(model, modelId) => `Transaction #${modelId}`} preview={Transaction} {...{ location, history, match }}>
      {Input({ name: 'description', displayName: 'Description' })}
      {NumberInputByDeposit({ name: 'value', displayName: 'Price' }, ({ model }) => model ? model.deposit_id : null)}
    </Form>
  )
}
