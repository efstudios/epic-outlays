import React, { useContext } from 'react'
import { formatDistanceToNow } from 'date-fns'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import parseISO from 'date-fns/parseISO'

import { DepositsContext, DepositContext, CurrenciesContext, CurrencyContext, DepositTypesContext, find } from '../../context'

export function TransactionGroup({ items, field }) {
  console.log('TransactionGroup', { items, field })
  const deposits = useContext(DepositsContext)
  const deposit = items && items.length ? find(deposits, items[0].deposit_id) : null

  const deposit_types = useContext(DepositTypesContext)
  const deposit_type = find(deposit_types, deposit.type_id)
  const icon = deposit_type.name

  const currencies = useContext(CurrenciesContext)
  const currency = deposit ? find(currencies, deposit.currency_id) : null

  const value = parseFloat(items.reduce((previous, item) => item.value + previous, 0) / currency.denominator).toFixed(2)
  const date = parseISO(field)
  return (
    <DepositContext.Provider value={deposit}>
      <CurrencyContext.Provider value={currency}>
        <li className='list-group-item'>
          <div className='row'>
            <div className='col-7 col-sm-4 col-lg-2'>
              <FontAwesomeIcon icon={icon} className='text-dark' /> {deposit.description}
            </div>
            <div className='text-muted col-4 col-lg-8 d-none d-sm-block' title={date}>
              {formatDistanceToNow(date)}
            </div>
            <div className='text-right text-muted col-5 col-sm-4 col-lg-2'>
              {value} {currency.sign}
            </div>
            <div className='text-muted col-12 d-block d-sm-none' title={date}>
              {formatDistanceToNow(date)}
            </div>
          </div>
        </li>
      </CurrencyContext.Provider>
    </DepositContext.Provider>
  )
}
