import React from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

export function TransactionCreateForm(props) {
  console.log('TransactionCreateForm', props)
  return (
    <button type='button' className='btn btn-light'>
      <FontAwesomeIcon icon={faPlus} />
    </button>
  )
}
