import React from 'react'
import { Page } from '../../utils/Page'

export function RegisterPage (props) {
  return (
    <Page title='Register'>
      Register!
    </Page>
  )
}
