import React from 'react'
import { onClickWrapper } from '../../utils/react'
import { Page } from '../../utils/Page'

export function LoginPage (props) {
  const onClick = (e) => {
    props.setCurrentUser({email: 'john.kolt@efstudios.org'})
  }

  return (
    <Page title='Login'>
      <a href='#' onClick={onClickWrapper(onClick)} className='btn btn-dark'>
        Login!
      </a>
    </Page>
  )
}
