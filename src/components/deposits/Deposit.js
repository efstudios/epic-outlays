import React, { useContext } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { CurrenciesContext, DepositTypesContext, find } from '../../context'

export function Deposit({ model }) {
  const currencies = useContext(CurrenciesContext)
  const deposit_types = useContext(DepositTypesContext)

  if (!model) return (
    <div className='text-center'>
      <div className='spinner-border text-dark' role='status'>
        <span className='sr-only'>Loading...</span>
      </div>
    </div>
  )

  const currency = model ? find(currencies, model.currency_id) : null
  const value = model ? parseFloat(model.value / currency.denominator).toFixed(2) : null

  const deposit_type = model ? find(deposit_types, model.type_id) : null
  const icon = deposit_type ? deposit_type.name : 'exclamation-triangle'

  console.log('Deposit', model, currency)
  return (
    <div>
      <span className='float-right text-muted'>{value + ' ' + currency.sign}</span>
      <FontAwesomeIcon icon={icon} className='text-dark' /> {model.description}
    </div>
  )
}
