import React from 'react'
import { Link } from 'react-router-dom'

import { Deposit } from './Deposit'
import { Page } from '../../utils/Page'
import { List } from '../../utils/List'

function DepositItem({ model }) {
  return (
    <Link className='list-group-item list-group-item-action' to={{ pathname: `/deposits/${model.id}`, state: { model } }}>
      <Deposit model={model} />
    </Link>
  )
}

export function DepositList(props) {
  console.log('DepositsList', props)
  return (
    <Page title='Deposits'>
      <List item={DepositItem} {...props} />
    </Page>
  )
}
