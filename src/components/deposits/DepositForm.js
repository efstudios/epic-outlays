import React, { useContext } from 'react'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { CurrenciesContext, DepositTypesContext, find } from '../../context'

import { Form } from '../../utils/Form'
import { Input, Autocomplete, NumberInput, NumberInputByCurrency } from '../../utils/Form/Input'
import { Deposit } from './Deposit'


export function DepositForm(props) {
  console.log('DepositForm', props)
  const currencies = useContext(CurrenciesContext).map((item) => ({ value: item.id, label: item.sign }))
  const depositTypes = useContext(DepositTypesContext).map((item) => ({ value: item.id, label: item.description, icon: item.name }))
  const { location, history, match, onSaveDeposit } = props
  const options = Object.keys(fas).map((i) => ({ value: fas[i].iconName, label: fas[i].iconName }))

  return (
    <Form title={({ model, modelId }) => `Deposit ${model ? model.description : '#' + modelId}`}
      preview={Deposit}
      onSave={onSaveDeposit}
      {...{ location, history, match }}>
      {Autocomplete({
        options,
        name: 'icon',
        displayName: 'Icon',
        renderOption: ({ value, label }) => (
          <div>
            <FontAwesomeIcon icon={value} className='icon text-dark' />
            <span style={{ paddingLeft: '10px' }}>
              {label}
            </span>
          </div>
        )
      })}
      {Input({ name: 'description', displayName: 'Description' })}
      {NumberInputByCurrency({ name: 'value', displayName: 'Value' }, ({ model }) => model ? model.currency_id : null)}
      {Autocomplete({
        options: currencies,
        name: 'currency_id',
        displayName: 'Currency'
      })}
      {Autocomplete({
        options: depositTypes,
        name: 'type_id',
        displayName: 'Deposit Type',
        renderOption: ({ value, label, icon }) => (
          <div>
            <FontAwesomeIcon icon={icon} className='icon text-dark' />
            <span style={{ paddingLeft: '10px' }}>
              {label}
            </span>
          </div>
        )
      })}
    </Form>
  )
}
