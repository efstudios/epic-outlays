import React, { useEffect } from 'react'
import { withRouter, NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faList, faBriefcase, faSignOutAlt, faUserPlus, faSignInAlt } from '@fortawesome/free-solid-svg-icons'

import { onClickWrapper } from '../../../utils/react'
import { fadeInDownAnimation } from '../../../utils/animation'

export function setTitle(title) {
  document.title = `${title} - Epic Outlays`
}

const HeaderLink = withRouter((props) => {
  if (!props.href) {
    return (
      <a className={`blog-nav-item ${props.addClass || ''}`} href='#' onClick={onClickWrapper(props.onClick)} aria-label={props.name}>
        <FontAwesomeIcon icon={props.icon} />
        <span className='text'>{props.name}</span>
      </a>
    )
  }

  useEffect(() => {
    const historyListener = (location) => {
      if (document.location.pathname === props.href) setTitle(props.name)
    }
    historyListener()

    return props.history.listen(historyListener)
  }, [])

  return (
    <NavLink className={`blog-nav-item ${props.addClass || ''}`} to={props.href} activeClassName='active' exact={props.exact} aria-label={props.name}>
      <FontAwesomeIcon icon={props.icon} />
      <span className='text'>{props.name}</span>
    </NavLink>
  )
})

function HeaderText(props) {
  return (
    <span className={`blog-nav-text ${props.addClass}`}>{props.text}</span>
  )
}

export function Header(props) {
  if (props.currentUser) {
    return (
      <header className={'blog-masthead ' + fadeInDownAnimation}>
        <div className='container'>
          <nav className='blog-nav'>
            <HeaderLink href='/transactions' icon={faList} name='Transactions' />
            <HeaderLink href='/deposits' icon={faBriefcase} name='Deposits' />

            <HeaderLink icon={faSignOutAlt} name='Logout' addClass='float-right' onClick={() => props.setCurrentUser(null)} />
            <HeaderText text={props.currentUser.email} addClass='float-right' />
          </nav>
        </div>
      </header>
    )
  } else {
    return (
      <header className={'blog-masthead ' + fadeInDownAnimation}>
        <div className='container'>
          <nav className='blog-nav'>
            <HeaderText text='Epic Outlays' />
            <HeaderLink href='/register' icon={faUserPlus} name='Register' addClass='float-right' />
            <HeaderLink href='/login' icon={faSignInAlt} name='Login' addClass='float-right' exact />
          </nav>
        </div>
      </header>
    )
  }
}
