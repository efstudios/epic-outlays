import React, { useState } from 'react'
import { Switch } from 'react-router'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import 'bootstrap'

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)

import { DepositsContext, DepositTypesContext, CurrenciesContext, update } from '../../context'

import { withProps } from '../../utils/react'

import { DepositList, DepositForm } from '../deposits'
import { TransactionList, TransactionForm } from '../transactions'
import { LoginPage, RegisterPage } from '../auth'

import { Header } from './Header'

import './App.less'

export default function App(props) {
  const [currentUser, setCurrentUser] = useState(props.currentUser)
  const [deposits, setDeposits] = useState(props.deposits)
  const [currencies, setCurrencies] = useState(props.currencies)
  const [deposit_types, setDepositTypes] = useState(props.deposit_types)
  const { transactions } = props

  const onSaveDeposit = (deposit) => setDeposits(update(deposits, deposit))

  if (!currentUser) {
    return (
      <Router>
        <Header currentUser={currentUser} setCurrentUser={setCurrentUser} />
        <main className='container'>
          <Switch>
            <Route exact path='/login' component={withProps(LoginPage, { setCurrentUser })} />
            <Route exact path='/register' component={RegisterPage} />
            <Redirect from='/' to='/login' />
          </Switch>
        </main>
      </Router>
    )
  }

  return (
    <CurrenciesContext.Provider value={currencies}>
      <DepositTypesContext.Provider value={deposit_types}>
        <DepositsContext.Provider value={deposits}>
          <Router>
            <Header currentUser={currentUser} setCurrentUser={setCurrentUser} />
            <main className='container'>
              <Switch>
                <Route exact path='/transactions' component={withProps(TransactionList, { transactions })} />
                <Route exact path='/transactions/:id' component={TransactionForm} />
                <Route exact path='/deposits' component={DepositList} />
                <Route exact path='/deposits/:id' component={withProps(DepositForm, { onSaveDeposit })} />
                <Redirect from='/' to='/transactions' />
              </Switch>
            </main>
          </Router>
        </DepositsContext.Provider>
      </DepositTypesContext.Provider>
    </CurrenciesContext.Provider>
  )
}
