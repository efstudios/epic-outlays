## Init dependencies

### JavaScript

```
npm install
```

### Python

```
virtualenv --python=python2 --no-site-packages env
```

### Database

See this manual:
https://bitbucket.org/efstudios/epic-outlays/wiki/DB%20Structure

## How to run test frontend server

Install dependencies

```
npm install
```

Run node server

```
npm start
```

Open browser

http://127.0.0.1:8080/

## Update database

```
UPDATE deposit_types SET name = 'money-bill' where name = 'cash';
UPDATE deposit_types SET name = 'id-card' where name = 'debit';
UPDATE deposit_types SET name = 'credit-card' where name = 'credit';
UPDATE deposit_types SET name = 'book' where name = 'other';
```