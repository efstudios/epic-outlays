const fs = require('fs')
const path = require('path')
const compression = require('compression')
const express = require('express')
const Handlebars = require('handlebars')
const addDays = require('date-fns/addDays')

const jsonApi = require('jsonapi-server')
const memoryHandler = new jsonApi.MemoryHandler()

jsonApi.setConfig({
  port: 8080,
  base: 'api',
  graphiql: false
})

const firstDate = new Date()

jsonApi.define({
  resource: 'deposits',
  handlers: memoryHandler,
  attributes: {
    // id: jsonApi.Joi.number().precision(0),
    id: jsonApi.Joi.string(),
    icon: jsonApi.Joi.string(),
    description: jsonApi.Joi.string(),
    currency: jsonApi.Joi.string(),
    value: jsonApi.Joi.number().precision(2)
  },
  examples: [
    {
      id: '1',
      type: 'deposits',
      icon: 'credit-card',
      description: 'Credit',
      currency: '₽',
      value: 35.3
    }, {
      id: '2',
      type: 'deposits',
      icon: 'wallet',
      description: 'Deposit',
      currency: '$',
      value: 44.3
    }, {
      id: '3',
      type: 'deposits',
      icon: 'money-bill-alt',
      description: 'Cash',
      currency: '€',
      value: 675.3
    }
  ]
})

jsonApi.define({
  resource: 'transactions',
  handlers: memoryHandler,
  attributes: {
    // id: jsonApi.Joi.number().precision(0),
    id: jsonApi.Joi.string(),
    date: jsonApi.Joi.date(),
    description: jsonApi.Joi.string(),
    amount: jsonApi.Joi.number().precision(2),
    deposit_id: jsonApi.Joi.string()
  },
  examples: [
    {
      id: '1',
      type: 'transactions',
      date: firstDate,
      description: 'Молоко "Простоквашино"',
      amount: 56.13,
      deposit_id: '1'
    }, {
      id: '2',
      type: 'transactions',
      date: firstDate,
      description: 'Хлебушек',
      amount: 44.3,
      deposit_id: '1'
    }, {
      id: '3',
      type: 'transactions',
      date: addDays(firstDate, -1),
      description: 'Пакет',
      amount: 4.49,
      deposit_id: '3'
    }
  ]
})

var server = jsonApi.getExpressServer()

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    return false
  }
  return compression.filter(req, res)
}
server.use(compression({ filter: shouldCompress }))

server.use('/dist', express.static(path.join(__dirname, 'dist')))

function renderIndex(req, res, next) {
  const indexTemplate = template = Handlebars.compile(fs.readFileSync(path.join(__dirname, 'views', 'index.html'), 'utf8'))
  const context = {}
  try {
    memoryHandler.search({ params: { type: 'deposits' } }, (_, deposits, depositsCount) => {
      const resultDeposits = []
      for (let i of deposits) {
        let { id, type, ...attributes } = i
        resultDeposits.push({ id, type, attributes })
      }
      context.deposits = JSON.stringify(resultDeposits)
      memoryHandler.search({ params: { type: 'transactions' } }, (_, transactions, transactionsCount) => {
        const resultTransactions = []
        for (let i of transactions) {
          let { id, type, ...attributes } = i
          resultTransactions.push({ id, type, attributes })
        }
        context.transactions = JSON.stringify(resultTransactions)
        res.send(indexTemplate(context))
      })
    })
  } catch (e) {
    console.error(e)
    throw e
  }
  // return res.sendFile(path.join(__dirname, 'views', 'index.html'))
}
server.get('/', renderIndex)
server.get('/transactions*', renderIndex)
server.get('/deposits*', renderIndex)
server.get('/login*', renderIndex)
server.get('/register*', renderIndex)

server.use('/robots.txt', function (req, res, next) {
  res.type('txt').send('')
})

jsonApi.start()
